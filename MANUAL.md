# Introduction
tile_gen generates SVG version 1.1 files representing artwork in the style of a decades old board game. The svg files are intended for a personal study project to implement the game as a python server with browser based javascript clients. (The client-server will not be distributed.)

tile_gen recognizes two kinds of data files:

* tile definition files describe how to arrange plan-view oriented landscape elements within a hexagon shaped space (the tile).
* tile family files define a grouping of individual tiles to have their graphics generated into a common svg file as an archive.

Both types of file are json formatted.

# Tile Family file
Used to group related tiles for use in the same svg archive.  FAM_NAME is a tile family name. 
T_NAME is the tile name. FAM_NAME and T_NAME need to match the FamilyName and TileName values within the tile definition file.

## General Organization
    {
        "TileFamily": FAM_NAME,
        "Members": [
            {"TileName": T_NAME, "FileName": F_Name},
            ...
        ]
    }

#Tile Definition file
Tile definition files describe which landscape elements to place and where to place them. Each definition file should contain data for two tile surfaces (sides): a front or Normal side and a back or Enchanted side. (The dual sided nature of the hexagon tiles was relevant to game mechanics.)

## General Organization

The tile definition file was originally intended for the server program and data above the TileGeography key is principally for server. Key-value pairs mutually used by the server and tile_gen are reflected here and in the sample files; server unique data has been suppressed. 

A tile definition file reflects this organization of upper level keys:

    {
        "Forbidden_Valley": {
            "TileName": "Forbidden Valley",
            "TileFamily": "valley",
            "Clearings": {
                "1": {...},
                "2": {...},
                ...
            },
            "TileGeography": { # artwork guidance data for tile_gen
                "Normal": {
                    "BackgroundColor": "WOODS_BACKGROUND",
                    "Clearings": {
                        "1": {...},
                        "2": {...},
                    },
                    "StraightPaths": [...],
                    "ArcPaths": [...],
                    "BaseTrees": [...],
                    "ConiferTrees": [...],
                    "BarrenTrees": [...],
                    "CaveEntrances": [...],
                    "RandomBoulders": {...}
                }
                "Enchanted": {
                    "BackgroundColor": "...",  | "MultiBackgrounds": [...]
                    ... # repeats the balance of Normal section key-value pairs
                }
            }
        }
    }

## Key Levels

### Level 1 Keys:
The solitary level 1 key is the tile name. It should be capitalized and embedded spaces replaced with an underscore.  As a matter of practice, I have been incorporating this key as part of the tile definition file name.

### Level 2 Keys:

The level 2 keys appearing above the TileGeography key are mutually used by tile_gen and the server program. 

*TileName* - contains the canonical Tile name. This value is used to label the tile at its 6 o'Clock (bottom) edge. tile_gen expects this value to differ from the level 1 key only in having space characters in lieu of the level 1 key's underscores.

*TileFamily* - The name of a group of related tiles. This name is entirely in lowercase. tile_gan can produce an svg archive containing defs for each side of each tile in a family. (The game mechanics differed by tile family and the tiles in each family had a 'family' resemblance.)

*Clearings* - Clearings are the playing "spaces" of the game board. They're identified by unique number but only within the tile. The Clearing numbers in the Clearings dictionary are themselves dictionary keys; the dictionaries define clearing specific information. Clearing numbers begin with 1 and are not necessarily sequential for a tile (e.g., the sample Awful Valley tile has clearings 1, 2, 4 and 5). Clearing numbers within this level must be synchronized to those within the TileGeography > Normal/Enchanted > Clearings dictionaries.

*TileGeography* - This is the root for landscape element and location data for drawing the tile's landscape artwork.

### The initially appearing Clearings dictionaries

The Clearing number dictionaries contain only two key-value pairs: ClrNum and ColorMagic.

  * The value of the ClrNum key must be an integer matching the surrounding dictionary's key.  

  * The ColorMagic key identifies a game rule specific 'color magic' name which tile_gen uses to determine a color for the 'exterior' of Cave Entrances when the Enchanted side of the tile is displayed. The three valid key values are Grey, Gold and Purple.

>     {
        "Forbidden_Valley": {
            "TileName": "Forbidden Valley",
            "TileFamily": "valley",
            "Clearings": {
                "1": {
                    "ClrNum": 1,
                    "ColorMagic": "Grey"
                },
                "2": {...},
                ...
            },
            ...
    }

### TileGeography and subordinate Keys
The TileGeography dictionary must contain the two keys Normal and Enchanted. The Normal side is the tile front and the Enchanted side is the back. Those two dictionaries define the landscape for each side of the tile. The kinds of data allowed within these two dictionaries are nearly identical. However, the arrangement of the landscape elements, including positioning of the clearings, may differ between a tile's two sides.


### Normal/Enchanted sub-Keys
Almost every Normal-Enchanted sub-key associates with a dictionary or a list. (Exception: the BackgroundColor key.) Each key's data pertains to a kind of drawing or landscape element. tile_gen lays down graphic layers in groups and the sub-keys coincide with the software's layering process.

The following sub-keys are required, even if their respective list or dictionary is empty:

  * BackgroundColor (or alternately, MultiBackgrounds under the Enchanted key)
  * Clearings
  * StraightPaths
  * ArcPaths

The remaining sub-keys are optional.

**Sub-Key Summary**

  * BackgroundColor or MultiBackgrounds - The value is a program constant name determining the hex tile's background color.

  * Clearings - Identifies the clearing's landscape setting and position on the tile.

  * StraightPaths - Defines a straight path's landscape setting with its start and end points. Bridge locations are also specified herein.

  * ArcPaths - Defines a curved path's landscape setting and the circular arc's path.

  * CaveEntrances - Defines where to place mine-shaft like cave entrance elements.

  * BaseTrees, ConiferTrees and-or BarrenTrees - Three categories of tree graphics. Within each category, there are specific named tree instances. These lists consist of dictionaries identifying the specific tree instance and its location.

  * RandomBoulders - Randomly place large rocks (boulders) across the tile. 

###Sub-Key Usage

**BackgroundColor, MultiBackgrounds**

The BackgroundColor key is valid under both Normal and Enchanted keys. It's four possible values determine the tile side's overall background color.
The value names and their associated landscape background colors are:

  * WOODS_BACKGROUND - green
  * GREY_MAGIC_BKGD - greyish-green
  * GOLD_MAGIC_BKGD - tan
  * PURPLE_MAGIC_BKGD - purple

Example:

    "Normal:" {
        "BackgroundColor": "WOODS_BACKGROUND",
        ...
    }

The MultiBackgrounds sub-key is only valid under the Enchanted sub-key. It is used to subdivide the tile into differently colored regions. (Clearings displayed within the colored regions are affected by different 'colors' of magic under the game rules.) 
MultiBackgrounds defines a list of dictionaries. Each dictionary has two keys: Color and FillPath. 

  * The Color key should be set to one of the four BackgroundColor value names.
  * The FillPath key defines a closed path as a list of points on the tile. The list of points is interpreted as connected line segments. It is not possible to use arcs in the fill path.

tile_gen fills each defined region with the color associated with the Color key value.

(The fill path is closed if the first and last listed locations coincide. See **Location Designations** for descriptions of how tile points and arc paths are defined within the file.

    "MultiBackgrounds": [
        {
            "Color": "PURPLE_MAGIC_BKGD",
            "FillPath": [
                {TILE_POINT_DEF key-value pairs},
                {TILE_POINT_DEF key-value pairs},
                ...
            ]
        },
        ...
    ]

**Clearings**

Similar to the initially appearing Clearings dictionary, this dictionary contains numbers in the range 1-6 as keys. Each clearing key has a dictionary as its value. The dictionary defines the type of clearing and its location (TILE_POINT_DEF key-value pairs, see **Location Designations**) on the tile.

    "2": {
        "ClrType": "woods",
        TILE_POINT_DEF key-value pairs
    }

Valid ClrType values are:

  * woods - tan circle with darker, curved wagon-wheel ruts
  * vale - identical to a woods clearing but surrounded by a darker background 
  * cave - a dark grey circle outlined by a broken white line.
  * mountain - identical to a woods clearing but surrounded by gray background and-or descending ridge-lines. 

Note that the TILE_POINT_DEF key-value pairs are not further segregated into a subordinate dictionary. Typically, clearing locations are designated as polar coordinates relative to the hexagon center.

**StraightPaths**

StraightPaths contains a list of dictionaries generally defining the start and end points of straight paths (exception: bridges).  Each path designates how to draw the path by its PathType key-value pair. 

All the following sub-keys are required:
  * PathType - the value determines what landscape element is intended and how the representative segment is drawn.
  * DrawOrder - determines whether a surface path is to be drawn before ("Early" value) or after ("Late" value) tunnel paths and cave clearings.
  * StartPt, EndPt - tile point locations determining the segment's route.

(The DrawOrder Early vs. Late bifurcation is to be eliminated thru reorganizing how tunnel and cave elements are drawn. Preserving DrawOrder key-value pairs in place will not disrupt processing of a future tool version.)

    {
        "PathType": "mountain",
        "DrawOrder": "Early",
        "StartPt": {
            TILE_POINT_DEF key-value pairs
        },
        "EndPt": {
            TILE_POINT_DEF key-value pairs
        }
    }

PathType may take any of the following values:

  * woods - draw a tan path dappled with dark wagon-wheel ruts.
  * vale - the same kind of path as a woods path, but surrounded by a darker background color than the balance of the tile background and outlined with scattered grey rocks.
  * cave - draws a subterranean tunnel. Drawn in dark gray with a broken white outline.
  * mountain - draws a margin of mountain top gray beside a tan surface path.
  * hidden - draws a hidden surface path which is narrower than a tunnel or woods/vale path, colored in dark brown with tan-gold rocks scattered along its length.
  * secret - draw a secret path which represents a secret tunnel. It has the same width as a hidden path, is colored black and has grey rocks scattered along its length.
  * bridge - this designates a bridge to be drawn instead of a path. The bridge's centerpoint and rotation are determined by StartPt and EndPt dictionaries. The bridge will be centered on the line segment and rotationally aligned with it. (NB: The implied segment length does not influence the bridge's dimensions.) There are four bridges whose names represent their approximate lengths in units of 10: black_bridge_NN where NN is 5, 6, 7 or 16.

Bridge placement example:

    {
        "PathType": "bridge",
        "BridgeName": "black_bridge_7",
        "StartPt": {
            TILE_POINT_DEF key-value pairs
        },
        "EndPt": {
            TILE_POINT_DEF key-value pairs
        }
    }

**ArcPaths**

ArcPaths contains a list of dictionaries defining circular-arc paths.  The kind of path (landscape element type) is designated by the PathType key. PathType may take any of the following values:

  * woods - indicating a tan path dappled with dark wagon-wheel ruts.
  * vale - the same kind of path as a woods path, but surrounded by a darker background color than the balance of the tile background and outlined with scattered grey rocks.
  * cave - indicating a subterranean tunnel. Drawn in dark gray with a broken white outline.
  * hidden - a hidden path is narrower than a tunnel or woods/vale path, colored in dark brown with tan/gold rocks scattered along its length.
  * secret - a secret path is a secret tunnel. It has the same width as a hidden path, is colored black and has grey rocks scattered along its length.

The extent of the arc is defined in the ARC_PATH_DEF series of key-value pairs. The particular method of defining the arc is determined by the PathDataCfg key-value pair. See **Arc Path Designations** for details.

    "ArcPaths": [
        {
            "PathType": "",
            "DrawOrder": "Late",
            "PathDataCfg": "",
            ARC_PATH_DEF key_value_pairs
        },
        ...
    ]

**CaveEntrances**

CaveEntrances contains a list of dictionaries. Each entry draws a mine entrance-like opening.  and is generally used where a woods or vale path meets a cave clearing. It has two sub-keys for locating the element:

  * CenterPt - The basis for locating the element. NB: the element is actually placed a clearing radius distance from the CenterPt. This was to simplify placement as this element is most commonly used at a clearing. 
  * Facing - this value determines the direction of offset from the CenterPt.

>     "CaveEntrances": [
        {
            "CenterPt": {
                TILE_POINT_DEF key-value pairs
            },
            "Facing": {
                TILE_POINT_DEF key-value pairs
            }
        },
    ]

It is possible to place Cave Entrances at other than clearings but determining the correct point is currently hamstrung by the intrinsic clearing radius offset from the declared center point.

Cave entrance 'exteriors' are ordinarily colored to match the tile background for the Normal tile side and the magic color of the clearing referenced by the CenterPt key for the Enchanted tile side. But entrances whose CenterPt is not a clearing number would have an undefined exterior color. In such cases, add a third key: ColorMagicClrNum. Assign the clearing number (an integer) whose magic color should be used when the entrance appears on the Enchanted side.

For example:

    {
        "ColorMagicClrNum": 3,
        "CenterPt": {
            "DataType": "PolarPrctg",
            "RadiusPrctg": "0.67",
            "Numerator": "9",
            "Denominator": "12"
                   },
		"Facing": {
            "DataType": "PolarPrctg",
            "RadiusPrctg": "0.7",
            "Numerator": "9",
            "Denominator": "12"
        }
    }


**Random Boulders**
The sole element in this dictionary is the Count key-value pair. The numeric value determines how many boulders the software places. The software will randomly choose among the several large rocks and randomly place them within a belt around the tile centerpoint. The belt range is from 35% to 70% of the tile radius. Boulders are placed after paths, clearings and mountains and may be covered by those and other elements. 

for example:

    "RandomBoulders": {
        "Count": "10"
    }

### Trees
There are three kinds (categories) of trees. More than one kind may be used; all are optional.

**BaseTrees**
Base trees are drawn as green-filled polygons studded with ellipses in various shades of green. There are 16 instances of this kind of tree. BaseTrees contains a list of dictionaries. Each dictionary has a Name key paired with a base tree name and a set of location designation key-value pairs to locate the tree element. The tree names are basetree_0 through basetree_15.

for example:

    "BaseTrees": [
        {
            "Name": "basetree_13",
            TILE_POINT_DEF key-value pairs
        },
        ...
    ]

**BarrenTrees**
Barren trees is a list of dictionaries for placing barren type trees. There are up to six variations of this tree kind, two for each radius. The software randomly determines the (limited) branch variations and determines which of two instances to place. The valid values for the Name key are barren_tree_25, barren_tree_39 and barren_tree_58. The number in the tree name refers to the element's radius.


    "BarrenTrees": [
        {
            "Name": "barren_tree_39",
            TILE_POINT_DEF key-value pairs
        },
        ...
    ]

**ConiferTrees**
Conifer trees contains a list of dictionaries for placing conifer type trees. The valid values for the Name key are conifer_14, conifer_17 and conifer_20.  The number refers to the element's radius. These trees are intended to look like green cones with branchlets.


    "ConiferTrees": [
        {
            "Name": "conifer_20",
            TILE_POINT_DEF key-value pairs
        },
        ...
    ]

###Location Designations
Annotated as TILE_POINT_DEF key-value pairs within the remainder of this manual, specific key-value pairs define point locations on the tile. There are several different sets of key-values pairs used to designate locations in different ways.

Every location designation has a **DataType** key which identifies how to interpret the remainder of its key-value pairs' set. Valid DataType values are:

  * ClrRef - refers to a clearing center.
  * EdgeRef - refers to the center of a tile edge.
  * CornerRef - refers to a corner of the hexagon-shaped tile.
  * PolarPrctg - refers to a point by relative polar coodinates.

**ClrRef**
A Clearing reference consists of two key-value pairs:

        "DataType": "ClrRef",
        "ClrNum": 3

The ClrNum key contains an integer matching one of the clearings previously defined in the Clearing dictionaries. The location coincides with the referenced clearing.

**EdgeRef**
An Edge reference consists of two key-value pairs:

        "DataType": "EdgRef",
        "EdgeNum": 5

The EdgeNum key contains an integer which refers to one of the hexagon's six edges. Tile edges are numbered in clockwise order starting with 1 at 12 o'Clock. The location coincides with the center point of the referenced tile edge.

**CornerRef**
A Corner reference consists of two key-value pairs:

        "DataType": "Corneref",
        "CornerNum": 5

The CornerNum key contains an integer which refers to one of the hexagon's six corners. Hex corners are numbered in clockwise order starting with 0 (zero) at 9 (nine) o'Clock.  The location coincides with the referenced hexagon corner.

**PolarPrctg**
A Polar Percentage set identifies a location in relative-radius polar coordinates and consists of four key-value pairs:

  * DataType - The type of point location data, use PolarPrcgt.
  * RadiusPrctg - percentage distance from tile centerpoint.
  * Numerator - integer factor of arc sweep for PI
  * Denominator - integer divisor of arc sweep for PI

For example:

        "DataType": "PolarPrctg",
        "RadiusPrctg": "0.6",
        "Numerator": "9",
        "Denominator": "6"

The polar coordinates in the example translate to:

  * radius = TILE_RADIUS x 60%
  * sweep angle = 9 x PI / 6

###Arc Path Designations
Arc paths are always circle arcs; elliptical arcs are not supported. Arc path data are designated by the phrase "ARC_PATH_DEF key_value_pairs" within this manual.
There are two patterns for defining curved paths determined by the **PathDataCfg** key-value pair:

  * **ThreePoints** - Three points on a circle, and
  * **PolarPrctgWithPts** - Centerpoint plus start and end points (of the circular arc)

**Important**: tile_gen requires that all arc paths be drawn in clockwise order. For a PolarPrctgWithPts arc, drawing commences at the StartPt and proceeds to the EndPt. For a ThreePoints arc, drawing proceeds from the RotStart angle to the RotEnd angle. Arc sweeps in excess of PI will draw incorrectly. 

The Three Points arc path format:

    {
        "PathDataCfg": "ThreePoints",
        "ArcRotation": {
            "RotStart": {
                "Numerator": "5",
                "Denominator": "6"
            },
            "RotEnd": {
                "Numerator": "7",
                "Denominator": "6"
            },
        },
        "StartPt": {
            TILE_POINT_DEF key-value pairs
        },
        "EndPt": {
            TILE_POINT_DEF key-value pairs
        },
    }

The Polar Percentage arc path format:

    {
        "PathDataCfg": "PolarPrctgWithPts",
        "CenterPt": {
            TILE_POINT_DEF key-value pairs
        },
        "StartPt": {
            TILE_POINT_DEF key-value pairs
        },
        "EndPt": {
            TILE_POINT_DEF key-value pairs
        },
    }


