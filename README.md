# Tile Generator (tile_gen)

tile_gen is a Python module to generate hexagon bounded artwork from a narrow library of predefined landscape elements. The name tile_gen (Tile Generator) relates to the artworks usage as 'gameboard tiles' for a larger, unpublished self-study project. The generated 'tiles' are SVG 1.1 files. The recipe for each 'tile' is a json formatted 'tile definition' file. The manual accompanying this readme describes the tile definition file's format and directives. There are a couple sample tile definition files in the src/tiles directory.


![High Pass tile with conifer trees](HighPass_sample.png)

Sample tile_gen output with mountains and tunnel.

# Invocation
The module resides in the src/tile_gen directory.
At present, it is mandatory to use one of the command line options -t, -e or -f with one or more input file names. Output defaults to a ./svg directory which must be present before invocation. 

tile_gen.tile_gen [-h] [-d] [-a] (-t | -e | -f) FILE_NAME [FILE_NAME ...] [-o OUTPUT_PATH]

  * The -t (--tile-name) option accepts a list of file names such as 'Awful_Valley_Tile.json Ledges_Tile.json'. The result are individual SVG files for the 'normal' (front) sides. 

  * The -e (--enchanted) option has identical usage to -t but the resulting SVG files display 'enchanted' tile sides.

  * The -f option expects one or more tile family files and builds an svg archive with a def entry for both normal and enchanted side of each tile of the family.

  * -o lets you set the output directory path (which must already exist). Default is ./svg.

  * -d activates svg debugging mode (pass thru to svgwrite module).

  * -a overwrites red dots on the start, end and radius center points of arcs; useful for 'dialing in' curved paths.

# Element Library
The library of landscape elements consists of:

  * Above ground 'surface' paths and circular 'clearings' decorated with wagon wheel-like rut markings.
  * Subterranean tunnels and cave clearings portrayed in 'X-ray' view.
  * Mine-like entrances denoting the threshold between surface and subterranean spaces.
  * Narrower, distinctly colored, debris strewn paths representing secret passages and forest short-cuts.
  * Circular mountains with radially emanating ridgelines surrounding mountain-top clearings.
  * A simple bridge in several pre-defined lengths primarily for path cross-overs sans intersection.
  * Darker background areas with marginal rock debris representing below-grade vales or flood-prone elevations.
  * Three categories of tree models.
  * Rocks and boulders.

![Awful Valley tile](Awful_Valley_Sample.png)

Sample output incorporating a debris lined vale

# Status
The tool is useful for my intended purpose of generating better looking graphics in svg format than my previous javascript HTML canvas targeted code. 

**Limitations**

  * Drawn graphics are 'incorrect" for complex cases where paths alternately cross over and under.
  * Lacks automated placement of trees across the visible background.
  * Automated placement of background boulders is unaware of other elements and they frequently disappear beneath paths and mountains.


**Bugs and deficiencies**

See the Release file.

**Aspirational notions**

  * A function to distribute graphic objects within a clipping-like region to a specified density (inter-object nearness). And another function to derive those region-paths from the JSON data files. These to support automatic placement of trees and boulders.
  * A function to draw ridgelines radially from an arbitrary path for more complex mountain shapes.
  * Algorithmic generation of rocks and base trees. (The rocks and base trees were manually drawn in inkscape and their svg data incorporated into tables; the software draws the conifer and barren trees.)
  * A function to create a zig-zag path by following an elliptical path. This to support a better looking 'base' tree.
  * Blurred mist or fog for the cave and tunnel elements.
  * Vary the radial distance of the 'fallen' rocks at the foot of mountain ridges to 'naturalize' the perimeter.
  * Use gradients in the vale feature.
  * Distribute the marginal vale debris so it follows the outer arc around clearings.
  * Algorithmically generate rectangular stone block images with suitable variations.
  * A version of the semi-circle following debris-distribution function to follow an arbitrary path. (To distribute stone blocks for 'ruin' landscapes.)
  * Patterned background in lieu of solid color.
  * Test cases.

# Origin
Summer 2022 I wrote the javascript code to generate gameboard tiles for the HTML canvas. The supported landscape elements were limited to the various paths plus the bridge. Everthing was "textured" by the rut drawing code.

![DOM-canvas-version](DOM-canvas-version.png)

Javascript HTML canvas tiles, September 2022.

The 'mountains' were merely gray colored circles with a few curved 'ruts' to imply elevation changes. There were no tile data files: I impatiently duplicated a ~50 line template-recipe that directed drawing each tile and hard-coded element location details (to sooner return to the 'real' work of the server program).

Three-hundred SLOC consisting of the wagon-rut and geometric functions were ported to Python at the beginning of April 2023.  By early May, all the new drawing and mountain code was written and running. The next six weeks was refactoring and small improvements.

This is my initial outing with SVG and using the svgwrite module was a substantial help.
