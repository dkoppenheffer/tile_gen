### 0.8 - Initial pre-release

Element library:

  * straight and curved tan surface paths decorated with brown ruts
  * tan surface clearings decorated with curving ruts
  * narrow, debris studded paths as short-cuts
  * straight and curved x-ray view tunnels
  * narrower, black debris studded paths as secret passages
  * cave (subterranean) clearings that open into intersecting tunnels
  * A mine-shaft like element to transition from surface to subterranean paths or clearings.
  * An algorithmically generated barren tree model with 1-2 random variations in three radii.
  * An algorithmically generated conifer tree model available in three radii instances.
  * A base tree model availble in 15 pre-defined instances.
  * Predefined rocks elements in sizes suitable as 'debris', (medium) rocks and boulders.
  * Gray 'mountain-top' margins surround mountain clearings and intervening 'mountain' paths.
  * Mountains consisting of 
    * varied ridge patterns radially extending from the mountain clearing,
    * ravine ends randomly decorateed with debris, and
    * base perimeter consisting of randomly rock-fall.
  * A black bridge in four pre-defined lengths.
  * A background shading effect designating depressed surface elevation (vales).


**Bugs and known deficiencies**

  * ThreePoint arcs are processed in two different and incompatible ways depending
upon the landscape element context.
  * Started but not completed rework to decouple drawing of cave clearings from
intersecting tunnels. As-is, the tool cannot correctly draw the tile if there
is more than one surface path crossing a tunnel in an alternating series.
Completion will obsolesce the tile definition file DrawOrder directive.
  * Lacks logic to subdivide the non-path, non-mountain background into 'fill
spaces' and lacks logic to populate fill-spaces with landscape elements such
as trees and boulders. Presently, you have to assign each tree instance to its location in the tile definition file.
  * The boulder placement function frequently places rocks beneath paths and
mountains or atop one another.
  * The 'base' trees resemble a green version of a spotted hard-candy from
childhood. That is, they look silly.
  * Needs a separate mode or directive to ease placement of Cave entrances not centered on a clearing.
  * A point and click editor for selecting and placing elements, especially curved paths.

### 0.8.1 -

  * Fix svg_trees base_tree bug: base_tree() was returning the last group
        (splotch group) added to tree group instead of returning tree group
        after adding splotch group. Base trees were not drawn. I assumed
        base_element.add() returned the base elem after add: it actually
        returns only the added elem. Don't combine return with add() method.
  * Re-ordered base_tree_factory so splotches defined before referencing
        in base_tree(). No apparent bug but prefer to generate dependancies
        prior to invocation.
  * Coding progress toward supporting long series of alternating surface and
        tunnel paths. Meanwhile, cave clearing outline is 'blocking' tunnel
        intersection.
  * Segregated the long black bridge from its siblings for def inclusion: it was
        two-fifths of the bridge def rows. Didn't want to get down to
        selecting individual bridges: valley family uses all three
        'normal' lengths.
  * README: Delete orphan sample output caption following Invocation section;
        insert Element Library header same location.
  * MANUAL: Cave Entrances was missing discussion of the optional ColorMagicClrNum; fixed.

