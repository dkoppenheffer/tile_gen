# copyright 2023 Daniel P. Koppenheffer
"""mr_unpk_data - Unpack and calculate coordinates from data files.

Point and arc data formats used in the *_Tile.json files.

Point type definitions (DataType):
PolarPrctg - a point defined by polar coordinates. Its data are in
    keys RadiusPrctg, Numerator and Denominator.
ClrRef - References a clearing of the tile. Data is in key ClrNum. Only
clearing numbers defined within the Clearings key are valid.
CornerRef - References a hex corner by number; zero is at 9 oClock.
    Data is in key CornerNum.
EdgeRef - References the center of an edge; starts with one at 12 oClock.
    Data is in key EdgeNum.


The tile radius (relative to a hex corner) is provided separately
(a constant) from the tile data files.

A polar point relative to the tile center point:
{
    "DataType": "PolarPrctg",       # Data type
    "RadiusPrctg": "0.2",           # Percent offset from tile center.
    "Numerator": "3",               # Numerator and Denominator; multiply
    "Denominator": "5"              # fraction by pi to obtain radian
                                    # sweep component.
}

A relative polar circular arc:
CenterPt determines this arc's center.
{
    "PathDataCfg": "PolarPrctgWithPts", # Data type
    "CenterPt": {                   # By not using "PolarPrctg" data type
        "DataType": "CornerRef",    # am using a different center for the
        "CornerNum": 2              # arc than tile center.
    },
    "StartPt": {
        "DataType": "EdgeRef",
        "EdgeNum": 2
    },
    "EndPt": {
        "DataType": "EdgeRef",
        "EdgeNum": 1
    }
},

A relative three-point circular arc:
    # Although the three points provide all the info needed to derive
    # start and end of arc sweep, the RotStart and RotEnd data are used
    # instead. This allows to "fudge" the 'as drawn' start and end
    # of the arc.
{
    "PathDataCfg": "ThreePoint",
    "ArcRotation": {
        "RotStart": {
            "Numerator": "1",       # As per PolarPrctg elements.
            "Denominator": "6"
        },
        "RotEnd": {
            "Numerator": "5",
            "Denominator": "6"
        }
    },
    # Three points needed to define the circle. The DataType for each
    # point may be dissimilar.
    "StartPt": {
        "DataType": "ClrRef",       # The tool calculates and caches all
        "ClrNum": 3                 # clearing coordinates so that you can
                                    # reference them by ID number.
    },
    "EndPt": {
        "DataType": "EdgeRef",      # Similar to ClrRefs, an EdgeRef refers
        "EdgeNum": 6                # to the center of a tile edge. Edge 1
                                    # is at 12 OClock.
    },
    "MidPt": {
        "DataType": "PolarPrctg",   # Third point def required needed to
        "RadiusPrctg": "0.65",      # define a circle. RadiusPrctg is
        "Numerator": "5",           # relative to tile center point, not
        "Denominator": "12"         # to the circle center.
    }
}

"""

import math

from tile_gen.svg_const import \
    CLEARING_RADIUS

from tile_gen.geom_utils import \
    angle_two_pts, find_circle, round_pt, abbrev_arc_ends, \
    frac_to_radians

from tile_gen.misc_draw import \
    transform_pt_def, calc_segment

def unpk_abbrev_3pt_arc_data(path_d):
    """Translate coords for 3 point arc to polar values.

    This also abbreviates the arc at both ends; used for paths to
    just overlap edge of clearings at both ends."""
    start_pt = round_pt(transform_pt_def(path_d["StartPt"]))
    end_pt = round_pt(transform_pt_def(path_d["EndPt"]))
    mid_pt = round_pt(transform_pt_def(path_d["MidPt"]))
    h_k_r = find_circle(start_pt, mid_pt, end_pt)
    ctr_pt = [round(h_k_r[0], 3), round(h_k_r[1], 3)]
    radius = round(h_k_r[2], 3)
    new_ends = abbrev_arc_ends(start_pt, end_pt, ctr_pt,
            CLEARING_RADIUS)
    start_rads = angle_two_pts(ctr_pt, new_ends[0])
    end_rads = angle_two_pts(ctr_pt, new_ends[1])
    if "AbbrevEnds" in path_d:
        if path_d["AbbrevEnds"] == "both":
            pass
        elif path_d["AbbrevEnds"] == "start":
            end_rads = round(angle_two_pts(ctr_pt, end_pt), 3)
        elif path_d["AbbrevEnds"] == "end":
            start_rads = round(angle_two_pts(ctr_pt, start_pt), 3)
        else:
            start_rads = round(angle_two_pts(ctr_pt, start_pt), 3)
            end_rads = round(angle_two_pts(ctr_pt, end_pt), 3)
    return tuple([ctr_pt, radius, start_rads, end_rads])

def unpk_abbrev_polar_arc_data(path_d):
    """Abbreviate arc end-points to edge of clearings.

    Prereq: start and end pts are clearing refs. 
    Abbreviates both ends of the arc by default. 
    If path_d contains an AbbrevEnds key its values determine if 
    neither or one end is abbreviated.
    AbbrevEnd values: "both" abbreviate both ends; default,
    "start": abbreviate only the starting end,
    "end": abbreviate only the ending end.
    Any other string turns off abbreviation for both ends.
    """
    ctr_pt = transform_pt_def(path_d["CenterPt"])
    cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
    new_ends = abbrev_arc_ends(cart_segm[0], cart_segm[1],
            ctr_pt, CLEARING_RADIUS)
    start_rads = round(angle_two_pts(ctr_pt, new_ends[0]), 3)
    end_rads = round(angle_two_pts(ctr_pt, new_ends[1]), 3)
    if "AbbrevEnds" in path_d:
        if path_d["AbbrevEnds"] == "both":
            pass
        elif path_d["AbbrevEnds"] == "start":
            end_rads = round(angle_two_pts(ctr_pt, cart_segm[1]), 3)
        elif path_d["AbbrevEnds"] == "end":
            start_rads = round(angle_two_pts(ctr_pt, cart_segm[0]), 3)
        else:
            start_rads = round(angle_two_pts(ctr_pt, cart_segm[0]), 3)
            end_rads = round(angle_two_pts(ctr_pt, cart_segm[1]), 3)
    radius = round(math.dist(ctr_pt, cart_segm[1]), 3)
    return tuple([ctr_pt, radius, start_rads, end_rads])

def unpack_arc_params(path_d) -> tuple:
    """Chooses appropriate process to calc arc path parameters."""
    if path_d["PathDataCfg"] == "PolarPrctgWithPts":
        return calc_polar_prctg_params(path_d)
    elif path_d["PathDataCfg"] == "ThreePoint":
        return translate_3pt_to_polar_coords(path_d)
    return None

def unpack_abbrev_arc_data(path_d) -> tuple:
    """Chooses appropriate process to calc arc path parameters.

    This shortens (abbreviates) the segment at both ends."""
    if path_d["PathDataCfg"] == "PolarPrctgWithPts":
        return unpk_abbrev_polar_arc_data(path_d)
    elif path_d["PathDataCfg"] == "ThreePoint":
        return unpk_abbrev_3pt_arc_data(path_d)

def calc_three_pt_arc_values(arc_data: dict):
    """Calculate sweep and three cartesian coordinates.

    arc_data is "ThreePoint" type arc data from data file.
    """
    val_data = {
        "StartRads": None,  "EndRads": None,
        "StartPt": [],      "EndPt": [],
        "MidPt": []
    }
    if arc_data["PathDataCfg"] != "ThreePoint":
        return False
    val_data["StartRads"] = frac_to_radians(
            arc_data["ArcRotation"]["RotStart"]["Numerator"],
            arc_data["ArcRotation"]["RotStart"]["Denominator"])
    val_data["EndRads"] = frac_to_radians(
            arc_data["ArcRotation"]["RotEnd"]["Numerator"],
            arc_data["ArcRotation"]["RotEnd"]["Denominator"])
    val_data["StartPt"] = round_pt(transform_pt_def(arc_data["StartPt"]))
    val_data["EndPt"] = round_pt(transform_pt_def(arc_data["EndPt"]))
    val_data["MidPt"] = round_pt(transform_pt_def(arc_data["MidPt"]))
    return val_data

def translate_3pt_to_polar_coords(path_d):
    """Translate coords for 3 point arc to polar values."""
    arc_d = calc_three_pt_arc_values(path_d)
    h_k_r = find_circle(arc_d["StartPt"], arc_d["MidPt"], arc_d["EndPt"])
    radius = round(h_k_r[2], 3)
    ctr_pt = [round(h_k_r[0], 3), round(h_k_r[1], 3)]
    return tuple([ctr_pt, radius, arc_d["StartRads"], arc_d["EndRads"]])

def calc_polar_prctg_params(path_d):
    """Calculate cartesian coords from PolarPrctgWithPts data.
    """
    cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
    ctr_pt = transform_pt_def(path_d["CenterPt"])
    start_radians = round(angle_two_pts(ctr_pt, cart_segm[0]), 3)
    end_radians = round(angle_two_pts(ctr_pt, cart_segm[1]), 3)
    radius = round(math.dist(ctr_pt, cart_segm[1]), 3)
    return tuple([ctr_pt, radius, start_radians, end_radians])

def unpk_polar_prctg_arc_pts(path_d):
    """Calculate center pt, radius and start, end pt coordinates."""
    cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
    ctr_pt = transform_pt_def(path_d["CenterPt"])
    radius = round(math.dist(ctr_pt, cart_segm[1]), 3)
    return tuple([ctr_pt, radius, cart_segm[0], cart_segm[1]])

def unpk_3pt_arc_pts(path_d):
    arc_d = calc_three_pt_arc_values(path_d)
    h_k_r = find_circle(arc_d["StartPt"], arc_d["MidPt"], arc_d["EndPt"])
    radius = round(h_k_r[2], 3)
    ctr_pt = [round(h_k_r[0], 3), round(h_k_r[1], 3)]
    return tuple([ctr_pt, radius, arc_d["StartPt"], arc_d["EndPt"]])

def unpack_arc_points(path_d) -> tuple:
    """Chooses appropriate process to calc arc path points."""
    if path_d["PathDataCfg"] == "PolarPrctgWithPts":
        return unpk_polar_prctg_arc_pts(path_d)
    elif path_d["PathDataCfg"] == "ThreePoint":
        return unpk_3pt_arc_pts(path_d)
    return None


