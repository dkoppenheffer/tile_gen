# copyright 2023 Daniel P. Koppenheffer
"""mr_misc_draw - Miscellaneous functions built up from svgwrite."""

import math

import svgwrite

from tile_gen.svg_const import \
    CLEARING_RADIUS, CLEARING_PTS, HEX_MIDPOINTS, HEX_CORNERS

from tile_gen.geom_utils import \
    calc_arc_pt, angle_two_pts, \
    calc_arc_sweep, calc_sweep_dist, display_bug_workaround, \
    abbrev_arc_ends, find_circle, round_pt, calc_polar_prctg_pt

def define_move(start_pt):
    """Return an SVG move string for SVG Path."""
    return f'M {start_pt[0]},{start_pt[1]}'

def begin_path(path_cmd, dwg):
    """Returns svgwrite path instance."""
    return dwg.path(d=path_cmd)

def move_to(ctr_pt, r, start_rads):
    """Return Path Move cmd relative to polar move from another point."""
    start_pt = calc_arc_pt(ctr_pt, r, start_rads)
    return define_move(start_pt)

def begin_move_to_path(ctr_pt, r, start_rads, dwg):
    """Return svgwrite path instance initialized with a move to cmd."""
    start_pt = calc_arc_pt(ctr_pt, r, start_rads)
    start_cmd = begin_path(define_move(start_pt), dwg)
    return start_cmd

def define_line(end_pt):
    """Return an SVG Line to string for SVG Path."""
    return f' L {end_pt[0]},{end_pt[1]}'

def line_to(start_pt, dist, start_rads):
    """SVG Line to pt at angle and distance from start_pt."""
    to_pt = calc_arc_pt(start_pt, dist, start_rads)
    return define_line(to_pt)

def emplace_element(element, ctr_pt, rotation, targ_grp):
    """Position an element for display in targ_grp.

    Rotation in degrees.
    """
    element.translate(ctr_pt[0], ctr_pt[1])
    element.rotate(rotation)
    targ_grp.add(element)

def place_element_w_offset(start_pt, face_pt, offset, elem_id, dwg):
    """Place element radially from start point."""
    angle = angle_two_pts(start_pt, face_pt)
    angle = display_bug_workaround(angle)
    place_pt = calc_arc_pt(start_pt, offset, angle)
    return place_defs_element(elem_id, place_pt, math.degrees(angle), dwg)

def place_defs_element(elem_nm, ctr_pt, rotation, dwg):
    """Place named element from defs cache rotated at ctr_pt."""
    elem_url = f'#{elem_nm}'
    elem = dwg.use(href=elem_url)
    elem.translate(ctr_pt)
    elem.rotate(rotation)
    return elem

def draw_polygon(pt_data, p_color, dwg):
    """Draw and fill polygon."""
    p_cmd = dwg.polygon(pt_data, stroke=p_color, stroke_width=1)
    p_cmd.fill(p_color, opacity=1)
    return p_cmd

def calc_lrg_arc_flag(start_rads, end_rads):
    """Return true if arc sweep is > half-circle."""
    if calc_arc_sweep(start_rads, end_rads) > math.pi:
        return 1
    return 0

def set_elip_arc_params(rx, ry, end_pt, large_arc_flag,
            x_rot=0, swp_flag=1):
    """Setup arc path for a circle or ellipse."""
    return f'A {rx} {ry} {x_rot} {large_arc_flag} ' \
            + f'{swp_flag} {end_pt[0]} {end_pt[1]}'

def define_round_arc(end_pt, r, start_rads, end_rads, swp_flg):
    """Returns path for a circular arc (ellipse)."""
    lrg_arc_flg = calc_lrg_arc_flag(start_rads, end_rads)
    return set_elip_arc_params(r, r, end_pt, lrg_arc_flg, 0, swp_flg)

def draw_arc_path(ctr_pt, r, start_rads, end_rads, \
                     arc_width, arc_color, dwg):
    """Draws a circular arc with specfied width and color.
    """
    end_pt = calc_arc_pt(ctr_pt, r, end_rads)
    arc_cmd = begin_move_to_path(ctr_pt, r, start_rads, dwg)
    swp_flag = 1
    arc_cmd.push(define_round_arc(end_pt, r,
            start_rads, end_rads, swp_flag))
    arc_cmd.stroke(arc_color, width=arc_width, linecap='round')
    # How do I prevent closure of the path? Workaround:
    return arc_cmd.fill(arc_color, opacity=0.0)

def draw_elliptical_arc(ctr_pt, r, start_rads, end_rads, dwg,
            skew=0, xrot=0):
    """Draw an elliptical arc; controls ellipse via skew parameter."""
    # skew is a prctg value: if skew is positive, increase X radius
    # over y radius; if negative, increase y radius. Units are one prct.
    sweep_flag = 1
    rx = r; ry = r
    if skew != 0:
        if skew < 0:
            ry *= (1 + (skew/100))
        if skew > 0:
            rx *= (1 + (skew/100))

    end_pt = calc_arc_pt(ctr_pt, r, end_rads * math.pi)
    large_arc_flag = calc_lrg_arc_flag(start_rads, end_rads)

    arc = begin_move_to_path(ctr_pt, r, start_rads, dwg)
    return arc.push(set_elip_arc_params(rx, ry, end_pt,
                large_arc_flag, xrot, sweep_flag))

def calc_segment(start_pos_d, end_pos_d, rel_to_pt=[0,0]):
    """Translate a segment.

    The _pos_d points are tile file data, not [x,y] coords.
    """
    return [
        transform_pt_def(start_pos_d, rel_to_pt),
        transform_pt_def(end_pos_d, rel_to_pt)
    ]

def transform_pt_def(pos_d, rel_pt=[0,0]):
    """Convert from tile def file point formats to cartesian."""
    if pos_d["DataType"] == "PolarPrctg":
        return calc_polar_prctg_pt(pos_d, rel_pt)
    if pos_d["DataType"] == "ClrRef":
        return CLEARING_PTS[int(pos_d["ClrNum"])]
    if pos_d["DataType"] == "EdgeRef":
        return HEX_MIDPOINTS[int(pos_d["EdgeNum"])]
    if pos_d["DataType"] == "CornerRef":
        return HEX_CORNERS[int(pos_d["CornerNum"])]

def create_straight_path(calc_segm, color, str_width, dwg, ln_cap='square'):
    """Create a straight path from tile def file data."""
    cart_segm = calc_segment(calc_segm[0], calc_segm[1])
    return create_cartes_path(cart_segm, color, str_width, dwg, ln_cap)

def create_cartes_path(cart_segm, color, str_width, dwg, l_cap='square'):
    """Draw straight path from cartesian coordinates."""
    str_path = dwg.line(tuple(cart_segm[0]), tuple(cart_segm[1]))
    return str_path.stroke(color, width=str_width, linecap=l_cap)


