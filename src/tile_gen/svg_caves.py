# copyright 2023 Daniel P. Koppenheffer
"""mr_svg_caves - Cave class."""

import math

import svgwrite

from tile_gen.svg_const import \
    PATH_WIDTH, HID_PATH_WIDTH, CLEARING_RADIUS, TILE_RADIUS, \
    CLEARING_PTS, PATH_BACKGROUND, WOODS_BACKGROUND, \
    HEX_MIDPOINTS, HEX_CORNERS, HEX_EDGES, \
    GOLD_MAGIC_BKGD, PURPLE_MAGIC_BKGD, GREY_MAGIC_BKGD, \
    GOLD_MAGIC_FRGD, PURPLE_MAGIC_FRGD, GREY_MAGIC_FRGD

from tile_gen.geom_utils import \
    calc_arc_pt, angle_two_pts, round_pt, perpendicular_angle, \
    abbrev_seg_ends, circumference, dual_circle_intersects

from tile_gen.misc_draw import \
    define_move, begin_path, define_line, define_round_arc, \
    calc_segment, create_cartes_path, draw_arc_path, transform_pt_def

from tile_gen.unpk_data import \
    translate_3pt_to_polar_coords, unpack_abbrev_arc_data, \
    unpack_arc_points

# Caves and tunnels (paths btwn cave clearings)
TUNNEL_BACKGROUND =  "#2E3436"
TUNNEL_FOREGROUND =  "dimgray"
TUNNEL_OUTLINE_WIDTH = 2

CAVE_PATH_START = "#280000"
CAVE_ENTR_BKGD = "#ad8d25"
CAVE_ENTR_FRGD = "#502d16"

SECRET_PSG_BACKGROUND = "black"
SECRET_PSG_FOREGROUND = "darkslategray"


class Caves:
    """Define caves clearings.

    Tunnels (cave paths) are not addressed herein.
    """
    def __init__(self):
        # Outside color at cave entrances matches magic for
        # enchanted tile side.
        self.EXT_COLORS = [WOODS_BACKGROUND, GREY_MAGIC_FRGD,
            PURPLE_MAGIC_FRGD, GOLD_MAGIC_FRGD]
        self.CAVE_EXT_NMS = ["woods", "Grey", "Purple", "Gold"]

    def cave_factory(self, dwg):
        """Create cave elements."""
        self.define_cave_clearing(CLEARING_RADIUS,
                        TUNNEL_BACKGROUND, TUNNEL_FOREGROUND , dwg)
        self.define_cave_entrance_grad(CAVE_PATH_START, PATH_BACKGROUND, dwg)
        for j in range(len(self.EXT_COLORS)):
            self.define_cave_exterior(self.EXT_COLORS[j],
                    self.CAVE_EXT_NMS[j], dwg)
        self.define_cave_entrance(CAVE_ENTR_BKGD, CAVE_ENTR_FRGD, dwg)

    def define_cave_clearing(self, r, bkgd_color, frgd_color, dwg):
        """Define cave clearing including dashed outline."""
        clr_struct = dwg.g(id='cave_clearing')
        circle = dwg.circle(tuple([0,0]), r)
        circle.fill(bkgd_color).stroke(bkgd_color, width=1)
        circle.dasharray(dasharray=[12, 8])
        clr_struct.add(circle)
        circle = dwg.circle(tuple([0,0]), r - 2)
        circle.fill(None, opacity=0)
        circle.dasharray(dasharray=[12, 8]).stroke(
                "white", width=3, opacity=1)
        # Notional: make mist in cave; would use frgd_color.
        clr_struct.add(circle)
        dwg.defs.add(clr_struct)

    def define_cave_entrance_grad(self, start_color, end_color, dwg):
        """Create gradient for shaded path entering cave."""
        grad = dwg.linearGradient(id='cave_entr_grad', opacity=1)
        grad.add_stop_color(offset="1%", color=start_color)
        grad.add_stop_color(offset="20%", color=start_color)
        grad.add_stop_color(offset="100%", color=end_color)
        dwg.defs.add(grad)

    def define_cave_exterior(self, surface_color, name, dwg):
        """Draw the ground level surrounding the framed cave entrance."""
        cv_ext_grp = dwg.g(id=f'cave_exterior_{name}')
        curve_start = calc_arc_pt([0,0], CLEARING_RADIUS -3,
                41 * math.pi / 24)
        ext_path = begin_path(define_move(curve_start), dwg)
        curve_end = calc_arc_pt([0,0], CLEARING_RADIUS -3,
                7 * math.pi / 24)
        ext_path.push(define_round_arc(curve_end, CLEARING_RADIUS,
                5 * math.pi / 12, 7 * math.pi / 12, 1))
        # up from end of curved base.
        next_pt = calc_arc_pt(curve_end, 3 * CLEARING_RADIUS / 5,
                angle_two_pts(curve_end, [0,0]))
        ext_path.push(define_line(next_pt))
        # Zag-zig down, up, down, up moving back above the starting pt end.
        for numerator in [20, 15, 20, 15]:
            next_pt = calc_arc_pt(next_pt, CLEARING_RADIUS / 5,
                    numerator * math.pi / 12)
            ext_path.push(define_line(next_pt))
        # line back to curve_start
        ext_path.push(define_line(curve_start))
        ext_path.stroke(surface_color, width=1)
        ext_path.fill(surface_color)
        cv_ext_grp.add(ext_path)
        dwg.defs.add(cv_ext_grp)

    def define_shaded_path(self, dwg):
        """This is the shaded portion of path going into the dark cave."""
        start_pt = round_pt([CLEARING_RADIUS / 9, 0])
        end_pt = round_pt([4 * CLEARING_RADIUS / 7, 0])
        str_line = create_cartes_path([start_pt, end_pt],
                "url(#cave_entr_grad)", PATH_WIDTH, dwg, 'butt')
        return str_line

    def embelish_timber_frame(self, start_pt, end_pt, lint_len,
            det_color, dwg):
        """Draw lines on cave entrance timbers for effect."""
        # dark lines ctr of lintel
        frm_embsh_grp = dwg.g(id='frame_emblsh_grp')
        # Point and angle numerator pairs: Diagonals where timbers join.
        pt_num = [[start_pt, 3], [end_pt, 9]]
        for j in range(len(pt_num)):
            begin_pt = calc_arc_pt(pt_num[j][0], 5,
                    pt_num[j][1] * math.pi / 4)
            stop_pt = calc_arc_pt(pt_num[j][0], -5,
                    pt_num[j][1] * math.pi / 4)
            frm_embsh_grp.add(create_cartes_path(
                    [begin_pt, stop_pt], det_color, 2, dwg, 'square'))
        # Length-wise splits in frame timbers.
        stop_pt = (start_pt[0], 10)
        frm_embsh_grp.add(create_cartes_path(
                    [start_pt, stop_pt], det_color, 2, dwg, 'square'))
        stop_pt = (end_pt[0], -6)
        frm_embsh_grp.add(create_cartes_path(
                    [end_pt, stop_pt], det_color, 2, dwg, 'square'))
        stop_pt = calc_arc_pt(start_pt, 4 * lint_len / 6, 1 * math.pi / 6)
        frm_embsh_grp.add(create_cartes_path(
                    [start_pt, stop_pt], det_color, 2, dwg, 'square'))
        base_pt = calc_arc_pt(end_pt, 4 * lint_len / 3, 11 * math.pi / 6)
        stop_pt = calc_arc_pt(base_pt, 4 * lint_len / 6, 5 * math.pi / 6)
        frm_embsh_grp.add(create_cartes_path(
                    [base_pt, stop_pt], det_color, 2, dwg, 'square'))
        return frm_embsh_grp

    def define_cave_entrance(self, frm_bkgd, frm_frgd, dwg):
        """Create framing around cave clearing external opening."""
        entr_grp = dwg.g(id="cave_enter")
        shaded_path = self.define_shaded_path(dwg)
        entr_grp.add(shaded_path)
        lint_len = 1 * CLEARING_RADIUS / 3
        lint_width = round(CLEARING_RADIUS / 6, 3)
        start_pt = round_pt([lint_width / 2, lint_len ])
        end_pt = round_pt([lint_width / 2, -lint_len ])
        str_line = create_cartes_path([start_pt, end_pt],
                frm_bkgd, lint_width, dwg)
        entr_grp.add(str_line)
        base_pt = calc_arc_pt(start_pt, 4 * lint_len / 3, 1 * math.pi / 6)
        str_line = create_cartes_path([start_pt, base_pt],
                frm_bkgd, lint_width, dwg)
        entr_grp.add(str_line)
        base_pt = calc_arc_pt(end_pt, 4 * lint_len / 3, 11 * math.pi / 6)
        str_line = create_cartes_path([end_pt, base_pt],
                frm_bkgd, lint_width, dwg, 'butt')
        entr_grp.add(str_line)
        embsh_grp = self.embelish_timber_frame(start_pt, end_pt, lint_len,
                frm_frgd, dwg)
        entr_grp.add(embsh_grp)

        dwg.defs.add(entr_grp)

    def draw_tunnel_arc(self, path_d, dwg):
        """Draw a curved cave (tunnel) path.

        Assumes end_pts are clearing centers so drawn length is abbreviated
        to reach edge of clearing.
        """
        arc_path_grp = dwg.g()
        ctr_pt, radius, start_rad, end_rad = \
                unpack_abbrev_arc_data(path_d)
        arc_path_grp.add(draw_arc_path(ctr_pt,
                radius, start_rad, end_rad,
                PATH_WIDTH, TUNNEL_BACKGROUND, dwg))
        return arc_path_grp

    def draw_tunnel_arc_outline(self, path_d, dwg):
        """Draw the dashed outline around a curved cave path.

        Assumes end_pts are clearing centers so drawn length is abbreviated
        to reach edge of clearing."""
        tunnel_outline = dwg.g()
        ctr_pt, radius, start_rad, end_rad = \
                unpack_abbrev_arc_data(path_d)
        tunnel_outline.fill("white").stroke("white", width=3)
        tunnel_outline.dasharray(dasharray=[12, 8])
        for ctr_ln_offset in [
                ((PATH_WIDTH / 2) + 1),
                -((PATH_WIDTH / 2) + 1)]:
            tunnel_outline.add(draw_arc_path(
                    ctr_pt, radius + ctr_ln_offset,
                    start_rad, end_rad, 3, "white", dwg))
        return tunnel_outline

    def draw_straight_tunnel(self, path_d, dwg):
        """Draw straight cave-tunnel segment."""
        cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
        if "AbbrevEnds" in path_d:
            abbrev_seg = abbrev_seg_ends(cart_segm, CLEARING_RADIUS,
                    path_d["AbbrevEnds"])
        else:
            abbrev_seg = abbrev_seg_ends(cart_segm, CLEARING_RADIUS)
        str_line = create_cartes_path(abbrev_seg, TUNNEL_BACKGROUND,
                PATH_WIDTH, dwg)
        str_path = dwg.g()
        str_path.add(str_line)
        return str_path

    def draw_straight_tunnel_outline(self, path_d, dwg):
        """Draws the white dashed outline around a straight cave path.

        Assumes end_pts are clearing centers so drawn length is abbreviated
        to reach edge of clearing.
        """
        outline_path = dwg.g()
        cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
        intersect_angle = angle_two_pts(cart_segm[0], cart_segm[1])
        perpen_angle = perpendicular_angle(intersect_angle)
        if "AbbrevEnds" in path_d:
            abbrev_seg = abbrev_seg_ends(cart_segm, CLEARING_RADIUS,
                    path_d["AbbrevEnds"])
        else:
            abbrev_seg = abbrev_seg_ends(cart_segm, CLEARING_RADIUS)
        for offset in [(PATH_WIDTH/2) + 1, -(PATH_WIDTH/2) -1]:
            begin_pt= calc_arc_pt(abbrev_seg[0], offset, perpen_angle)
            end_pt= calc_arc_pt(abbrev_seg[1], offset, perpen_angle)
            str_line = create_cartes_path([begin_pt, end_pt], "white",
                    3, dwg)
            str_line.dasharray(dasharray=[8, 12])
            outline_path.add(str_line)
        return outline_path

    def erase_cave_clr_outline(self, r, bkgd_color, ctr_pt, facing, dwg):
        """Erase dash line where tunnel enters clearing."""
        dist_tot = circumference(CLEARING_RADIUS)
        prctg_dist = round(PATH_WIDTH / dist_tot, 3)
        start_rad = facing - (math.pi * prctg_dist)
        end_rad = facing + (math.pi * prctg_dist)
        return draw_arc_path(ctr_pt, r - 2, start_rad, end_rad, 4,
                bkgd_color, dwg)

    def mk_clr_intersect(self, path_d_start, path_d_end):
        """Return clearing edge pt and angle where tunnel intersects."""
        new_intersect = {}
        new_intersect["CtrPt"] = CLEARING_PTS[int(path_d_start["ClrNum"])]
        new_intersect["Angle"] = angle_two_pts(
                transform_pt_def(path_d_start),
                transform_pt_def(path_d_end))
        return new_intersect

    def open_straight_tunnel_intersect(self, start_pt_d, end_pt_d, dwg):
        """Mediate cave clr outline segment erasure for straight intersect.
        """
        t_intersect = self.mk_clr_intersect(start_pt_d, end_pt_d)
        return self.erase_cave_clr_outline(
                        CLEARING_RADIUS, TUNNEL_BACKGROUND,
                        t_intersect["CtrPt"], t_intersect["Angle"],
                        dwg)

    def open_arc_tunnel_intersect(self, path_d, tile_cmds, dwg):
        """Mediate cave clr outline segment erasure for curved intersect.
        """
        arc_ctr, arc_r, start_pt, end_pt = unpack_arc_points(path_d)
        if path_d["StartPt"]["DataType"] == "ClrRef":
            tile_cmds.add(self.calc_arc_clr_intersect(
                    start_pt, arc_ctr, arc_r, end_pt, dwg))
        if path_d["EndPt"]["DataType"] == "ClrRef":
            tile_cmds.add(self.calc_arc_clr_intersect(
                    end_pt, arc_ctr, arc_r, start_pt, dwg))


    def calc_arc_clr_intersect(self, clr_ctr, arc_ctr, arc_r,
            end_pt, dwg):
        """ Return clearing edge pt and angle where tunnel arc intersects.

        This is the curved path sibling to mk_clr_intersect."""
        intersect_opts = dual_circle_intersects(
                clr_ctr, CLEARING_RADIUS, arc_ctr, arc_r)
        # Compare each intersection to dist to the other end of path.
        # Select the nearer intersection; the further pt is not on the arc.
        if math.dist(intersect_opts[0], end_pt) < \
                math.dist(intersect_opts[1], end_pt):
            facing = angle_two_pts(clr_ctr, intersect_opts[0])
        else:
            facing = angle_two_pts(clr_ctr, intersect_opts[1])
        return self.erase_cave_clr_outline(
                CLEARING_RADIUS, TUNNEL_BACKGROUND,
                clr_ctr, facing, dwg)


