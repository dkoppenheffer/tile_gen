# copyright 2023 Daniel P. Koppenheffer
"""Generate hex tiles styled after the Magic Realm boardgame artwork.

The current segregation of drawing paths into 'Early' surface paths,
then caves and tunnels and lastly, 'Late' surface paths doesn't support
complex overlayments such as for the Borderland tile. Due to the
atomization of drawing cave clearings, tunnels and outlines, it would
require breaking the Borderland elements into three passes. And,
had Borderland a normal tunnel passage between clearings 4 and 5, it
would be impossible to draw correctly, regardless of number of passes.
I need to segregate drawing tunnels from drawing cave clearings by
having the cave clearing code open the pathway into the clearing at each
tunnel intersection. Then I can discard the Early/Late bifurcation and
restore the simpler ordering: draw all tunnels firstly, then overlay
surface paths and lastly lay down cave clearings (without interdependency
of caves clearings and tunnels being drawn together).
"""

import math
import json
import pathlib
import argparse
import sys
from enum import IntFlag, auto

import numpy
import svgwrite

from tile_gen.svg_const import \
    SVG_PATH, TILES_PATH, SKIP_INDICES, \
    JSON_SFX, LABEL_STYLE, CLRING_STYLE, \
    HEX_MIDPOINTS, HEX_CORNERS, HEX_EDGES, CLEARING_PTS, \
    PATH_WIDTH, HID_PATH_WIDTH, CLEARING_RADIUS, TILE_RADIUS, \
    HIDDEN_FOREGROUND, HIDDEN_BACKGROUND, \
    PATH_FOREGROUND, PATH_BACKGROUND, \
    GOLD_MAGIC_BKGD, PURPLE_MAGIC_BKGD, GREY_MAGIC_BKGD, \
    GREY_MAGIC_VALE, TILE_LABEL_COLOR, ALT_LABEL_COLOR, \
    WOODS_BACKGROUND, VALE_FACTOR, VALE_BACKGROUND, \
    BRIDGE_FOREGROUND, BRIDGE_BACKGROUND

from tile_gen.geom_utils import \
    calc_arc_pt, angle_two_pts, \
    perpendicular_angle, compute_mid_pt, round_pt

from tile_gen.misc_draw import \
    place_element_w_offset, place_defs_element, \
    draw_arc_path, calc_segment, transform_pt_def, \
    create_straight_path, create_cartes_path, draw_polygon

from tile_gen.unpk_data import unpack_arc_params

from tile_gen.wgn_ruts import WagonRuts

from tile_gen.svg_caves import Caves, \
    SECRET_PSG_FOREGROUND, SECRET_PSG_BACKGROUND, TUNNEL_BACKGROUND

from tile_gen.svg_trees import Trees, \
    SPLOTCH_COLORS

from tile_gen.svg_rocks import Rocks

from tile_gen.svg_mntns import RidgeLines, Mountains, \
    MNTN_BACKGROUND

from tile_gen.align_help import mark_curves


class BlackBridge:
    """Define and place Black Bridge elements."""

    # Predefined bridge lengths: three 'normal' and one long.
    # The slightly elongnated normals are to accomodate acute angle 
    # crossings.
    FIVE = 54
    SIX = 64
    SEVEN = 74
    SIXTEEN = 164

    def __init__(self, bridge_color, slat_color):
        self.BRG_COLOR = bridge_color
        self.SLAT_COLOR = slat_color

        # Params for bridge_factory method
        self.NORM_SET = [BlackBridge.FIVE,
            BlackBridge.SIX,
            BlackBridge.SEVEN]
        self.LONG_SET = [BlackBridge.SIXTEEN]

    def place_black_bridge(self, brg_nm, align_seg, dwg):
        """Draw a bridge from dwg.defs on tile.

        Bridge will be aligned and centered on align_seg."""
        # Determine bridge placement rotation angle.
        cart_segm = calc_segment(align_seg[0], align_seg[1])
        angle = angle_two_pts(cart_segm[0], cart_segm[1])
        # Bridge pix is aligned vertically. (should have drawn horz!)
        # Since 0 degrees start at 3 O Clock, always add 90 to rotation.
        rot_angle = round(math.degrees(angle + (math.pi * 1 / 2)), 3)
        mid_pt = round_pt(compute_mid_pt(cart_segm[0], cart_segm[1]))
        bridge = place_defs_element(brg_nm, mid_pt, rot_angle, dwg)
        return bridge

    def create_black_bridge(self, v_len, h_width, dwg):
        """Create and store a bridge with dimensions in dwg.defs."""
        units = math.trunc(v_len/10)
        dom_id = f'black_bridge_{units}'
        bridge_grp = dwg.g(id=dom_id)

        rect = dwg.rect(size=(h_width, v_len))
        rect.fill(self.BRG_COLOR)
        rect.stroke(self.BRG_COLOR, width=1)
        bridge_grp.add(rect)
        # Draw grey "slats".
        slat_width = 12
        for idx in range(units):
            slats = dwg.rect(insert=((slat_width/2) , (idx * 10) + 5),
                    size=(h_width - slat_width, 4))
            slats.fill(self.SLAT_COLOR)
            slats.stroke(self.SLAT_COLOR, width=1)
            bridge_grp.add(slats)

        # Draw "guide" arms.
        for corner in [
                [4, 4, 330],
                [h_width - 4, 4, 30],
                [h_width - 4, v_len - 4, 150],
                [4, v_len - 4, 210]
        ]:
            bridge_gd = dwg.line((corner[0], corner[1]),
                    (corner[0], corner[1] - 24))
            bridge_gd.stroke(self.BRG_COLOR, width=7)
            bridge_gd.rotate(corner[2], (corner[0], corner[1]))
            bridge_grp.add(bridge_gd)

        # Center
        bridge_grp.translate(-h_width/2, -v_len/2)
        dwg.defs.add(bridge_grp)

    def bridge_factory(self, brg_set, dwg):
        """Create black bridges with specified lengths in dwg.defs."""
        width = 48
        for length in brg_set:
            bridges.create_black_bridge(length, width, dwg)

def draw_vale_arc(path_d, side_up, dwg):
    """Draws a curved vale path."""
    arc_path_grp = dwg.g()
    ctr_pt, radius, start_rad, end_rad = unpack_arc_params(path_d)
    vale_color = get_vale_color(side_up)
    arc_path_grp.add(draw_arc_path(ctr_pt, radius, start_rad, end_rad,
            PATH_WIDTH * VALE_FACTOR, vale_color, dwg))
    return arc_path_grp

def get_vale_color(side_up):
    if side_up == "Enchanted":
        return GREY_MAGIC_VALE
    return VALE_BACKGROUND

def draw_vale_clearings(t_data, side_up: str, tile_cmds, dwg):
    """Draw a vale surrounding a surface clearing."""
    # Alt. Use a linear gradient and clip it with a circle!
    vale_color = get_vale_color(side_up)
    for clr_num, clr in t_data[side_up]["Clearings"].items():
        clr_ctr_pt = CLEARING_PTS[int(clr_num)]
        if clr["ClrType"] == "vale":
            vale = dwg.circle(clr_ctr_pt, 1.5 * CLEARING_RADIUS)
            vale.stroke(vale_color)
            vale.fill(vale_color)
            tile_cmds.add(vale)

def draw_vale_debris(path_d, factor, dwg):
    """Lines a valley tile vale with stones at random intervals."""
    cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
    angle = angle_two_pts(cart_segm[0], cart_segm[1])
    perp_angle = perpendicular_angle(angle)
    offset = PATH_WIDTH * factor / 2
    debris_out = dwg.g()
    for adj in [offset, -offset]:
        start_pt = calc_arc_pt(cart_segm[0], adj, perp_angle)
        end_pt = calc_arc_pt(cart_segm[1], adj, perp_angle)
        debris_out.add(rocks.straight_path_debris([start_pt, end_pt],
                40, 10, 51, "tunnel_debris", dwg))
    return debris_out

def draw_vale_debris_arc(path_d, r_factor, dwg):
    """Distribute small rocks along the periphery of a vale."""
    debris_out = dwg.g()
    ctr_pt, radius, start_rad, end_rad = unpack_arc_params(path_d)
    offset = PATH_WIDTH * r_factor / 2
    for adj in [offset, -offset]:
        adj_radius = radius + adj
        debris_out.add(rocks.curved_path_debris(start_rad, end_rad,
                ctr_pt, adj_radius, 40, 10, 51, "tunnel_debris", dwg))
    return debris_out

def get_vale_gradient(side_up):
    if side_up == "Enchanted":
        return 'vale_grad_ench'
    return 'vale_grad_norm'

def create_vale_gradient(dwg):
    """Create the gradient for a vale."""
    grad = dwg.linearGradient(id='vale_grad_norm',
                gradientUnits="userSpaceOnUse", opacity=1)
    grad.add_stop_color(offset="1%", color=WOODS_BACKGROUND)
    grad.add_stop_color(offset="50%", color=VALE_BACKGROUND)
    grad.add_stop_color(offset="100%", color=WOODS_BACKGROUND)
    dwg.defs.add(grad)
    grad = dwg.linearGradient(id='vale_grad_ench',
                gradientUnits="userSpaceOnUse", opacity=1)
    grad.add_stop_color(offset="1%", color=GREY_MAGIC_BKGD)
    grad.add_stop_color(offset="50%", color=GREY_MAGIC_VALE)
    grad.add_stop_color(offset="100%", color=GREY_MAGIC_BKGD)
    dwg.defs.add(grad)

def draw_hex_w_bkgd(corners: list, dwg: svgwrite.Drawing, bkgd=None):
    """Outline hexagon; apply color fill if provided."""
    hex_cmd = dwg.polygon(corners, stroke='black', stroke_width=6)
    if bkgd:
        hex_cmd.fill(bkgd, opacity=1)
    else:
        hex_cmd.fill(None, opacity=0)
    return hex_cmd

def paint_multi_bkgds(t_side, tile_cmds, dwg):
    """Used to set background color for only a part of hex tile."""
    for elem in t_side["MultiBackgrounds"]:
        pts = []
        bkgd_color = globals() [elem["Color"]]
        for pt_def in elem["FillPath"]:
            pts.append(transform_pt_def(pt_def))
        tile_cmds.add(draw_polygon(pts, bkgd_color, dwg))

def set_bkgd_color(t_s_data, tile_cmds, dwg):
    """Set one or more hexagon background colors."""
    bkgd_color = 'green'
    if "BackgroundColor" in t_s_data.keys():
        bkgd_color = globals() \
                [t_s_data["BackgroundColor"]]
    tile_cmds.add(draw_hex_w_bkgd(HEX_CORNERS, dwg, bkgd_color))
    if "MultiBackgrounds" in t_s_data.keys():
        paint_multi_bkgds(t_s_data, tile_cmds, dwg)
    return bkgd_color

def define_surface_clearing(clr_r, bkgd_color, dwg):
    """Define woods or mountain clearing."""
    clr_struct = dwg.g(id='surface_clearing')
    circle = dwg.circle(tuple([0,0]), clr_r)
    circle.fill(bkgd_color).stroke(bkgd_color, width=1, opacity=1)
    clr_struct.add(circle)
    clr_struct.add(wagon_ruts.draw_clearing_ruts([0,0], clr_r, dwg))
    dwg.defs.add(clr_struct)

def draw_surface_arc(path_d, dwg):
    """Draws a curved surface path."""
    arc_path_grp = dwg.g()
    ctr_pt, radius, start_rad, end_rad = unpack_arc_params(path_d)
    arc_path_grp.add(draw_arc_path(ctr_pt, radius, start_rad, end_rad,
            PATH_WIDTH, PATH_BACKGROUND, dwg))
    arc_path_grp.add(wagon_ruts.compute_arc_rut_tracks(
            start_rad, end_rad, ctr_pt, radius, dwg))
    return arc_path_grp

def draw_hidden_path_arc(path_d, bkgd_color, debris_kind, dwg):
    """Draws a curved hidden path or secret passage."""
    arc_path_grp = dwg.g()
    ctr_pt, radius, start_rad, end_rad = unpack_arc_params(path_d)
    arc_path_grp.add(draw_arc_path(ctr_pt, radius, start_rad, end_rad,
            HID_PATH_WIDTH, bkgd_color, dwg))

    arc_path_grp.add(rocks.curved_path_debris(start_rad, end_rad, ctr_pt,
            radius, HID_PATH_WIDTH, 10, 31, debris_kind, dwg))
    return arc_path_grp

def draw_tunnels(t_data, side_up: str, tile_cmds, dwg):
    """Draws tunnels: straight and curved.

    Outlining happens elsewhere.
    """
    for path_d in t_data[side_up]["StraightPaths"]:
        if path_d["PathType"] == "cave":
            tile_cmds.add(caves.draw_straight_tunnel(path_d, dwg))
    for path_d in t_data[side_up]["ArcPaths"]:
        if path_d["PathType"] == "cave":
            tile_cmds.add(caves.draw_tunnel_arc(path_d, dwg))

def draw_tunnel_outlines(t_data, side_up: str, tile_cmds, dwg):
    """Draw tunnel outlines. Cave clearings excepted."""
    for path_d in t_data[side_up]["StraightPaths"]:
        if path_d["PathType"] == "cave":
            tile_cmds.add(caves.draw_straight_tunnel_outline(path_d, dwg))
    for path_d in t_data[side_up]["ArcPaths"]:
        if path_d["PathType"] == "cave":
            tile_cmds.add(caves.draw_tunnel_arc_outline(path_d, dwg))

def draw_cave_clearings(t_data, side_up: str, tile_cmds, dwg):
    """Draw and label all cave clearings (interior space)."""
    for clr_num, clr in t_data[side_up]["Clearings"].items():
        if clr["ClrType"] == "cave":
            clr_ctr_pt = CLEARING_PTS[int(clr_num)]
            tile_cmds.add(dwg.use(href="#cave_clearing", insert=clr_ctr_pt))
            label_pt = tuple([clr_ctr_pt[0] - 5, clr_ctr_pt[1] + 7])
            tile_cmds.add(dwg.text(str(clr_num),
                    insert=label_pt, style=CLRING_STYLE))

def carve_tunnel_openings(t_data_side, tile_cmds, dwg):
    """'Erase' cave clearing outline at tunnel entrances."""
    for path_d in t_data_side["StraightPaths"]:
        # Straight tunnels
        if path_d["PathType"] == "cave":
            if path_d["StartPt"]["DataType"] == "ClrRef":
                tile_cmds.add(caves.open_straight_tunnel_intersect(
                        path_d["StartPt"], path_d["EndPt"], dwg))
            if path_d["EndPt"]["DataType"] == "ClrRef":
                tile_cmds.add(caves.open_straight_tunnel_intersect(
                        path_d["EndPt"], path_d["StartPt"], dwg))
    for path_d in t_data_side["ArcPaths"]:
        if path_d["PathType"] == "cave":
            if path_d["StartPt"]["DataType"] == "ClrRef":
                caves.open_arc_tunnel_intersect(path_d, tile_cmds, dwg)

def draw_cave_entrances(t_data, side_up: str, color_cache, tile_cmds, dwg):
    """Place cave clearing entrances."""
    if "CaveEntrances" in t_data[side_up]:
        for entr_d in t_data[side_up]["CaveEntrances"]:
            ext_kind = "woods"
            if side_up == "Enchanted":
                if "ClrNum" in entr_d["CenterPt"]:
                    ext_kind = color_cache[int(entr_d["CenterPt"]["ClrNum"])]
                elif "ColorMagicClrNum" in entr_d.keys():
                    ext_kind = color_cache[int(entr_d["ColorMagicClrNum"])]
            exterior_nm = f'cave_exterior_{ext_kind}'

            clr_ctr_pt = transform_pt_def(entr_d["CenterPt"])
            facing_pt = transform_pt_def(entr_d["Facing"])
            cave_ext = place_element_w_offset(clr_ctr_pt, facing_pt,
                    3, exterior_nm, dwg)
            tile_cmds.add(cave_ext)
            entrance = place_element_w_offset(clr_ctr_pt, facing_pt,
                    1 * CLEARING_RADIUS / 2, "cave_enter", dwg)
            tile_cmds.add(entrance)

def cache_color_magic(gm_t_data):
    """Map magic-color by clearing number."""
    color_cache = {}
    for elem in gm_t_data["Clearings"].values():
        color_cache[elem["ClrNum"]] = elem["ColorMagic"]
    return color_cache

def draw_surface_clearings(t_data, side_up: str, tile_cmds, dwg):
    """Draw and label non-cave clearings."""
    for clr_num, clr in t_data[side_up]["Clearings"].items():
        clr_ctr_pt = CLEARING_PTS[int(clr_num)]
        if clr["ClrType"] in ["woods", "mountain", "vale"]:
            tile_cmds.add(dwg.use(href="#surface_clearing",
                    insert=clr_ctr_pt))
            # Need to measure text length for x axis centering.
            label_pt = tuple([clr_ctr_pt[0] - 5, clr_ctr_pt[1] + 7])
            tile_cmds.add(dwg.text(str(clr_num),
                    insert=label_pt, style=(CLRING_STYLE)))

def underlay_straight_path(path_d, color, factor, dwg):
    """Used to generate 'vale' effect and mountain-top path to next peak.
    """
    str_path = dwg.g()
    str_path.add(create_straight_path(
            [path_d["StartPt"], path_d["EndPt"]],
            color, PATH_WIDTH * factor, dwg, 'butt'))
    return str_path

def draw_straight_hidden_path(path_d, bkgd_color, debris_kind, dwg):
    """Draw straight hidden path."""
    cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
    str_line = create_cartes_path(cart_segm, bkgd_color, HID_PATH_WIDTH, dwg)
    str_path = dwg.g()
    str_path.add(str_line)
    debris_field = rocks.straight_path_debris([cart_segm[0], cart_segm[1]],
                HID_PATH_WIDTH, 10, 21, debris_kind, dwg)
    str_path.add(debris_field)
    return str_path

def draw_straight_path(path_d, bkgd_color, p_width, dwg):
    """Draw a straight surface path with its ruts."""
    cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
    str_path = dwg.g()
    str_path.add(create_cartes_path(
            cart_segm, bkgd_color, p_width, dwg))
    str_path.add(wagon_ruts.draw_straight_path_ruts(
            cart_segm, PATH_WIDTH, dwg))
    return str_path


def draw_straight_paths(path_d, side_up, tile_cmds, dwg):
    """Add a straight path element to tile_cmds."""
    if path_d["PathType"] == "mountain":
        # Rocky underlayment for mountain path.
        tile_cmds.add(underlay_straight_path(
                path_d, MNTN_BACKGROUND, 1.3, dwg))
    # A 'vale' is the flood-plain on the Valley tiles.
    # The vale is signified by a darker background color or gradient.
    if path_d["PathType"] == "vale":
        vale_color = get_vale_color(side_up)
        # Need to swap w and h of vale in order for gradient
        # to run in needed direction. A short, tile wide path.
        #vale_color = dwg.use(f'#vale_gradient')
        #vale_color.rotate(90)
        tile_cmds.add(underlay_straight_path(
                path_d, vale_color, VALE_FACTOR, dwg))
        tile_cmds.add(draw_vale_debris(path_d, VALE_FACTOR, dwg))
    if path_d["PathType"] in ["woods", "mountain"]:
        tile_cmds.add(draw_straight_path(path_d,
                PATH_BACKGROUND, PATH_WIDTH, dwg))
    if path_d["PathType"] == "hidden":
        tile_cmds.add(draw_straight_hidden_path(
                path_d, HIDDEN_BACKGROUND, "path_debris", dwg))
    if path_d["PathType"] == "secret":
        tile_cmds.add(draw_straight_hidden_path(
                path_d, SECRET_PSG_BACKGROUND, "tunnel_debris", dwg))

def draw_curved_paths(path_d, side_up, tile_cmds, dwg):
    """Add a curved path element to tile_cmds."""
    if path_d["PathType"] == "woods":
        tile_cmds.add(draw_surface_arc(path_d, dwg))
    if path_d["PathType"] == "vale":
        tile_cmds.add(draw_vale_arc(path_d, side_up, dwg))
        tile_cmds.add(draw_vale_debris_arc(path_d, VALE_FACTOR, dwg))
    if path_d["PathType"] == "hidden":
        tile_cmds.add(draw_hidden_path_arc(
                path_d, HIDDEN_BACKGROUND, "path_debris", dwg))
    if path_d["PathType"] == "secret":
        tile_cmds.add(draw_hidden_path_arc(
                path_d, SECRET_PSG_BACKGROUND, "tunnel_debris", dwg))

def draw_surface_paths(t_data, side_up: str, tile_cmds, dr_time, dwg):
    """Draw woods, mountain, hidden and valley-vale paths."""
    for path_d in t_data[side_up]["StraightPaths"]:
        if path_d["DrawOrder"] == dr_time:
            draw_straight_paths(path_d, side_up, tile_cmds, dwg)
    for path_d in t_data[side_up]["ArcPaths"]:
        if path_d["DrawOrder"] == dr_time:
            draw_curved_paths(path_d, side_up, tile_cmds, dwg)

def draw_bridges(t_data, side_up: str, tile_cmds, dr_time, dwg):
    """Draw each bridge on side_up side of tile."""
    for path_d in t_data[side_up]["StraightPaths"]:
        if path_d["DrawOrder"] == dr_time:
            if path_d["PathType"] == "bridge":
                str_path = bridges.place_black_bridge(path_d["BridgeName"],
                        [path_d["StartPt"], path_d["EndPt"]], dwg)
                tile_cmds.add(str_path)

def init_clr_coords(t_data, side_up: str):
    """Initialize the table of tile clearing coordinates."""
    for clr_num, clr in t_data[side_up]["Clearings"].items():
        CLEARING_PTS[int(clr_num)] = transform_pt_def(clr)

def init_draw_obj(name, output_path, dbg_flg) -> svgwrite.Drawing:
    """Create tile specific drawing element."""
    file_nm_out = f'{output_path}/{name}.svg'
    return svgwrite.Drawing(filename=file_nm_out,
                            profile="full", debug=dbg_flg)

def set_tile_dim(dwg):
    """Set and return the tile graphic dimensions."""
    vb_width = (2 * TILE_RADIUS) + 10
    vb_height = vb_width * 0.98
    dwg.viewbox(minx=0, miny=0, width=vb_width, height=vb_height)
    return [vb_width, vb_height]

def center_tile(t_dim, tile_cmds):
    """Put the cartesion origin, [0,0], at the hexagon center."""
    tile_cmds.translate(t_dim[0]/2, t_dim[1]/2)

def define_hex_clipper(dwg):
    clip_path = dwg.clipPath(id="hex_clipper")
    clip_path.add(dwg.polygon(HEX_CORNERS))
    dwg.defs.add(clip_path)

def clip_tile_edges(tile_cmds):
    """Clip hexagon edges."""
    tile_cmds.__setitem__("clip-path", "url(#hex_clipper)")

def label_tile(t_name, bkgd_color, dwg):
    """Apply tile name to bottom of tile."""
    midpt = HEX_MIDPOINTS[4].copy()
    edge_margin = 15
    midpt[1] -= edge_margin

    # Offset left of center N pixels per label-character.
    leftoffset = len(t_name) * 5
    xy = tuple([midpt[0] - leftoffset, midpt[1]])
    name = t_name.replace("_", " ").upper()
    label_color = TILE_LABEL_COLOR
    if bkgd_color == GOLD_MAGIC_BKGD:
        label_color = ALT_LABEL_COLOR
    color_setting = f'fill: {label_color}; stroke: {label_color}; '
    label = dwg.text(name, insert=xy,
            style=(color_setting + LABEL_STYLE))
    return label

def create_tile_side_grp(dwg, name, side_up):
    """Define a named group per tile side."""
    sfx1 = "_norm"
    if side_up == "Enchanted":
        sfx1 = "_ench"
    dom_id = f'{name}{sfx1}'
    return dwg.g(id=dom_id)

def create_elem_defs(t_data, side, dwg):
    """Create elements needed by tile defs within dwg.defs."""
    def_elem_flags = DefFlags.NONE
    dwg_elem_flags = updt_dwg_elem_flags(t_data, side, def_elem_flags)
    build_std_tile_elements(dwg)
    build_opt_tile_elements(dwg_elem_flags, dwg)

def draw_tile(name, t_data, side_up, dwg):
    """Create SVG code to draw the front or back of an MR tile.

    General Order of graphic layers:
    hex clipping.
    hexagon outline.
    tile background color
    tile background pattern
    mountains (with their own clipping)
    early surface paths (those that enter cave clearings)
    secret passages (so they do not draw over cave dotted surround)
    cave clearings including outline
    tunnel outlines
    tunnel background
    late (the balance of) surface paths and hidden paths
    surface clearings
    bridges
    labels
    """

    print(f' \'{name}\'', end="")
    t_name = name.replace(" ", "_")

    tile_cmds = create_tile_side_grp(dwg, t_name, side_up)
    tile_dim = set_tile_dim(dwg)
    center_tile(tile_dim, tile_cmds)
    clip_tile_edges(tile_cmds)
    bkgd_color = set_bkgd_color(t_data["TileGeography"][side_up],
            tile_cmds, dwg)
    init_clr_coords(t_data["TileGeography"], side_up)
    mountains.draw_mountains(t_data, side_up, tile_cmds, dwg)
    draw_vale_clearings(t_data["TileGeography"], side_up, tile_cmds, dwg)
    rocks.draw_rand_boulders(t_data["TileGeography"], side_up,
            tile_cmds, dwg)
    if not len(SKIP_INDICES):
        draw_surface_paths(t_data["TileGeography"], side_up, tile_cmds,
                "Early", dwg)
    # The drawing order of tunnel outlines, cave clearings and tunnel
    # bodies is premised upon:
    # a) the cave clearing overwriting the tunnel outline protrusion into
    # the clearing space, and
    # b) the abbreviated, rounded tunnel body end overwriting the cave
    # clearing outline while stopping short of the clearing center thus
    # maintaining the clearing ID visibility.
    draw_tunnel_outlines(t_data["TileGeography"], side_up, tile_cmds, dwg)
    draw_tunnels(t_data["TileGeography"], side_up, tile_cmds, dwg)
    if not len(SKIP_INDICES):
        draw_surface_paths(t_data["TileGeography"], side_up, tile_cmds,
                "Late", dwg)
    draw_surface_clearings(t_data["TileGeography"], side_up, tile_cmds, dwg)
    draw_cave_clearings(t_data["TileGeography"], side_up,
            tile_cmds, dwg)
    carve_tunnel_openings(t_data["TileGeography"][side_up], tile_cmds, dwg)
    magic_colors = cache_color_magic(t_data)
    draw_cave_entrances(t_data["TileGeography"], side_up, magic_colors,
            tile_cmds, dwg)
    draw_bridges(t_data["TileGeography"], side_up, tile_cmds, "Late", dwg)
    trees.plant_trees(t_data["TileGeography"], side_up, tile_cmds, dwg)

    # Re-draw hex border.
    tile_cmds.add(draw_hex_w_bkgd(HEX_CORNERS, dwg))

    # Label tile.
    tile_cmds.add(label_tile(name, bkgd_color, dwg))

    return tile_cmds


def init_objects():
    global rng
    rng = numpy.random.default_rng()

    global wagon_ruts, rocks, trees, ridge_lines
    global mountains, bridges, caves
    wagon_ruts = WagonRuts(PATH_WIDTH, PATH_FOREGROUND, rng)
    caves = Caves()
    rocks = Rocks(rng)
    trees = Trees(rng)
    ridge_lines = RidgeLines()
    mountains = Mountains(rng, rocks)
    bridges = BlackBridge(BRIDGE_BACKGROUND, BRIDGE_FOREGROUND)

def read_in_json_dictionary(filename):
    """ read json and return map or list """
    filepath = pathlib.Path(filename)
    if filepath.exists():
        fd = filepath.open("rt")
        file_in = fd.read()
        fd.close()
        return json.loads(file_in)
    return False

def build_std_tile_elements(dwg):
    """Create defs for mandatory tile elements."""
    # tunnel and path debris
    define_hex_clipper(dwg)
    rocks.debris_factory(dwg)
    define_surface_clearing(CLEARING_RADIUS, PATH_BACKGROUND, dwg)

def build_opt_tile_elements(def_elem_flags, dwg):
    """Create defs for optional tile elements."""
    if DefFlags.VALE_FLG in def_elem_flags:
        create_vale_gradient(dwg)
        rocks.rock_factory(dwg)
    if DefFlags.CAVE_FLG in def_elem_flags:
        caves.cave_factory(dwg)
    if DefFlags.MNTN_FLG in def_elem_flags:
        ridge_lines.ridge_factory(dwg)
        rocks.rock_factory(dwg)
    if DefFlags.BRIDGE_FLG in def_elem_flags:
        bridges.bridge_factory(bridges.NORM_SET, dwg)
    if DefFlags.BRIDGE_LONG_FLG in def_elem_flags:
        bridges.bridge_factory(bridges.LONG_SET, dwg)
    if DefFlags.BARREN_TR_FLG in def_elem_flags:
        trees.barren_tree_factory(dwg)
    if DefFlags.CONIFER_TR_FLG in def_elem_flags:
        trees.conifer_tree_factory(dwg)
    if DefFlags.BASE_TR_FLG in def_elem_flags:
        trees.base_tree_factory(SPLOTCH_COLORS, dwg)

class DefFlags(IntFlag):
    NONE = 0
    VALE_FLG = auto()
    MNTN_FLG = auto()
    BASE_TR_FLG = auto()
    BARREN_TR_FLG = auto()
    CONIFER_TR_FLG = auto()
    BRIDGE_FLG = auto()
    BRIDGE_LONG_FLG = auto()
    CAVE_FLG = auto()

def defs_reqd_by_clr_type(t_data, side_up, def_flags):
    """Create dwg.def elements needed for some ClrType values."""
    flg_out = def_flags
    for clearing in t_data["TileGeography"][side_up]["Clearings"].values():
        if clearing["ClrType"] == "vale":
            flg_out = flg_out | DefFlags.VALE_FLG
        if clearing["ClrType"] == "mountain":
            flg_out = flg_out | DefFlags.MNTN_FLG
        if clearing["ClrType"] == "cave":
            flg_out = flg_out | DefFlags.CAVE_FLG
    return flg_out

def defs_reqd_by_path_type(t_data, side_up, def_flags):
    """Which path specific elements need to be in the dwg.defs."""
    for geo_elem, g_data in t_data["TileGeography"][side_up].items():
        if geo_elem in ["StraightPaths", "ArcPaths"]:
            for path in g_data:
                if path["PathType"] == "bridge":
                    if path["BridgeName"] == "black_bridge_16":
                        def_flags = def_flags | DefFlags.BRIDGE_LONG_FLG
                    else:
                        def_flags = def_flags | DefFlags.BRIDGE_FLG
    return def_flags

def defs_reqd_by_tree_type(t_data, side_up, def_flags):
    """Which tree elements need to be in dwg.defs."""
    for geo_elems in t_data["TileGeography"][side_up]:
        if geo_elems == "BarrenTrees":
            def_flags = def_flags | DefFlags.BARREN_TR_FLG
        if geo_elems == "ConiferTrees":
            def_flags = def_flags | DefFlags.CONIFER_TR_FLG
        if geo_elems == "BaseTrees":
            def_flags = def_flags | DefFlags.BASE_TR_FLG
    return def_flags

def read_tile_fam(fam_nm):
    """Read tile family file, return tile files list."""
    tile_fam_d = read_in_json_dictionary(fam_nm)
    if not tile_fam_d:
        print(f'Could not locate the tile family file {fam_nm}.'
                + " Aborting.")
        sys.exit(1)
    return tile_fam_d

def updt_dwg_elem_flags(t_data, side_up, dwg_elem_flags):
    """Determine which elements need to be in dwg.defs."""
    dwg_elem_flags = defs_reqd_by_clr_type(t_data, side_up, dwg_elem_flags)
    dwg_elem_flags = defs_reqd_by_path_type(t_data, side_up, dwg_elem_flags)
    dwg_elem_flags = defs_reqd_by_tree_type(t_data, side_up, dwg_elem_flags)
    return dwg_elem_flags

def build_tile_fam(fam_mems, dir_p, dwg):
    """Build svg file of defs for tiles in family."""
    # First pass: Determine which element defs to build.
    dwg_elem_flags = DefFlags.NONE
    for elem in fam_mems:
        print(elem)
        t_data = read_in_json_dictionary(dir_p + "/" + elem["FileName"])
        if not t_data:
            print('no t_data')
        nm_w_underscore = elem["TileName"].replace(" ", "_")
        print(nm_w_underscore)
        for side in ["Normal", "Enchanted"]:
            dwg_elem_flags = updt_dwg_elem_flags(
                    t_data[nm_w_underscore], side, dwg_elem_flags)

    build_std_tile_elements(dwg)
    build_opt_tile_elements(dwg_elem_flags, dwg)

    # Second pass: Build each tile def
    for elem in fam_mems:
        t_data = read_in_json_dictionary(dir_p + "/" + elem["FileName"])
        nm_w_underscore = elem["TileName"].replace(" ", "_")
        for side in ["Normal", "Enchanted"]:
            dwg.defs.add(draw_tile(elem["TileName"],
                    t_data[nm_w_underscore], side, dwg))

def to_filename(tile_nm):
    tile_name = tile_nm.replace(" ", "_")
    return f'{tile_name}_Tile{JSON_SFX}'

def mk_view_tiles(file_names, args, side_up):
    """Create SVG files to be viewed (outside the game).
    This implements the --tile-name option."""
    for file_nm_in in file_names:
        t_data = read_in_json_dictionary(file_nm_in)
        if not t_data:
            print(f'Could not read tile file {file_nm_in}.'
                    + " Aborting.")
            sys.exit(1)
        # There should be only one top-level key in a tile definition file.
        f_keys = t_data.keys()
        nm_w_underscore = list(f_keys)[0]
        dwg = init_draw_obj(nm_w_underscore,
                args.output_path, args.debug_flag)
        create_elem_defs(t_data[nm_w_underscore], side_up, dwg)
        tile_cmds = draw_tile(t_data[nm_w_underscore]["TileName"],
                t_data[nm_w_underscore], side_up, dwg)
        if args.align_help_flag:
            mark_curves(t_data[nm_w_underscore]["TileGeography"],
                side_up, tile_cmds, dwg)
        dwg.add(tile_cmds)
        dwg.save(pretty=True, indent=4)
    print("")

def mk_gm_defs(args):
    """Create svg archive of defs suitable for in game usage.

    This implements the --tile-family option."""
    for file_nm_in in args.file_name:
        p = pathlib.Path(file_nm_in)
        dir_p = str(p.parent)
        print(dir_p)
        t_fam_in = read_tile_fam(file_nm_in)
        fam_nm = t_fam_in["TileFamily"]
        print(f'Build tile family \'{fam_nm}\':', end="")
        file_nm = fam_nm + "_TileFamily"
        dwg = init_draw_obj(file_nm, args.output_path, args.debug_flag)
        build_tile_fam(t_fam_in["Members"], dir_p, dwg)
        dwg.save(pretty=True, indent=4)
    print("")

def init_cmd_ln_parser():
    """Initialize command line parsing."""
    parser = argparse.ArgumentParser(description=
            "Magic Realm SVG tile generator.",
            epilog="Creates Scalar Vector Graphic files of either " \
                    + "individual tiles (viewable) or defs of tiles "
                    + "by family for game usage. The tile or family "
                    + "name option is required. Expects tile definition "
                    + "files in ../../json/tiles. Writes svg files to "
                    + "existing ../svg directory. ",
             prog="tile_gen.tile_gen")
    excl_grp = parser.add_mutually_exclusive_group()
    excl_grp.add_argument('-t', '--tile-name',
            action="store_true", dest='normal_side',
            help='Build viewable SVG files for the list of tile names. '
            + 'All tile images will be of the "normal" side. '
            + 'Example: -t Ledges_Tile.json High_Pass_Tile.json.')
    excl_grp.add_argument('-e', '--enchanted',
            action="store_true", dest='enchanted_side',
            help='Build viewable SVG files for the enchanted '
            + 'side of the listed tile names. '
            + 'Example: -e Linden_Woods_Tile.json')
    excl_grp.add_argument('-f', '--tile-family',
            action="store_true", dest='fam_name',
            help='Build SVG defs files for the list of tile family names. '
            + 'Both normal and enchanted tile sides are generated. Each '
            + 'tile family is written to a separate svg file. '
            + 'Example, to generate two tile families: -f Cave_Family.json '
            + 'Woods_Family.json')
    parser.add_argument('file_name', type=str, nargs="+",
            action="extend")
    parser.add_argument('-o', '--output-path',
            dest='output_path', default=SVG_PATH,
            help='Path to existing directory to receive svg files. Extant '
            + 'svg files will be overwritten. Default path is ./svg')
    parser.add_argument('-d', '--debug',
            dest='debug_flag', action='store_true',
            help='Activates SVG debugging. Example: -d')
    parser.add_argument('-a', '--align-help',
            dest='align_help_flag', action='store_true',
            help='Marks arc start, end and center points with a red dot '
                    + ' as an aid to connecting landscape elements. '
                    + ' Example: -a')
    return parser

def main():
    parser = init_cmd_ln_parser()
    args_in = parser.parse_args()
    if not (args_in.normal_side or
            args_in.enchanted_side or
            args_in.fam_name) or \
            not args_in.file_name:
        parser.print_usage()
        sys.exit(1)

    init_objects()

    if args_in.normal_side:
        mk_view_tiles(args_in.file_name, args_in, "Normal")
    elif args_in.enchanted_side:
        mk_view_tiles(args_in.file_name, args_in, "Enchanted")
    elif args_in.fam_name:
        mk_gm_defs(args_in)


if __name__ == '__main__':
    main()
