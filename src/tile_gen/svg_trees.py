# copyright 2023 Daniel P. Koppenheffer
"""mr_svg_trees - Trees class - define svg trees."""

import math


from tile_gen.geom_utils import \
calc_arc_pt, perpendicular_angle, arc_lens_in_half_circle

from tile_gen.misc_draw import \
    begin_move_to_path, define_line, line_to, \
    place_defs_element, transform_pt_def

#####################
# Tree constants

BARREN_TREE_COLOR = "#202020"
BARREN_TREE_RADII = [25, 39, 58]

CONIFER_RADII = [14, 17, 20]
CONIFER_LIGHT = '#007200'
CONIFER_DARK = '#004800'
CONIFER_CHEVRON = '#003800'

BASE_TREE_COLOR = "#007800"
# Other Base Tree data at end of file.

class Trees:
    """Define and draw various kinds of trees.

    Trees are used as landscape (e.g., Valley tile barren trees) and
    in quantity to imply forest covering. Base trees were manually
    generated, alas."""
    def __init__(self, rand_ng):
        self.rng = rand_ng
        # Conifer tree constants.
        self.chev_offset = 3
        self.chev_len = 5

    def make_splotch(self, splotch_idx, spl_color, dwg):
        """Create color splotch for tree crown.

        Ellipses defined in the TREE_LIPSES table.
        """
        splotch = dwg.ellipse(r=TREE_LIPSES[splotch_idx], center=[0,0])
        splotch.stroke(spl_color, width=1)
        return splotch.fill(spl_color)

    def base_tree(self, name, nm_data, bkgd_color, dwg):
        """Generate one splotched tree crown."""
        # Dimensions depend on the specific tree.
        tree_url = f'basetree_{name}'
        tree_grp = dwg.g(id=tree_url)
        tree_path = dwg.path(d=("m", 0, 0))
        tree_path.push(f' {nm_data}')
        # Bogus 'shadow' surrounding tree's canopy.
        tree_path.stroke("#004000", width=2)
        tree_path.fill(bkgd_color, opacity=1)
        tree_grp.add(tree_path)
        tree_grp.add(self.splotch_tree(name, dwg))
        return tree_grp

    def splotch_tree(self, pattern_idx, dwg):
        """Apply color splotches to form a tree's crown."""
        splotch_grp = dwg.g()
        for data in BASE_TREE_DEFS[pattern_idx]:
            spl_idx, ctr_pt, shade_idx, rot = data
            # Final [1:] shaves '#' from color code, "#rrbbgg"
            splotch_nm = f'splotch_{spl_idx}_{SPLOTCH_COLORS[shade_idx][1:]}'
            splotch_grp.add(place_defs_element(splotch_nm, ctr_pt, rot, dwg))
        return splotch_grp

    def conifer_tree_factory(self, dwg):
        """Create conifer trees of various radii in dwg.defs."""
        for r in CONIFER_RADII:
            self.make_conifer_grad(r, CONIFER_LIGHT, CONIFER_DARK, dwg)
        self.define_chevron(self.chev_offset, self.chev_len,
                    CONIFER_CHEVRON, dwg)
        for r in CONIFER_RADII:
            self.make_conifer(r, dwg)

    def barren_tree_factory(self, dwg):
        """Create barren trees in dwg.defs."""
        for r in BARREN_TREE_RADII:
            for idx in range(2):
                self.barren_tree(r, idx, dwg)

    def base_tree_factory(self, color_chart, dwg):
        """Create base trees in dwg.defs. """
        for idx in range(len(TREE_LIPSES)):
            #for c_idx in range(len(color_chart)):
            # Need to narrow choices in tree canopy color chart!
            for c_idx in range(4, len(SPLOTCH_COLORS)):
                splotch = self.make_splotch(idx, color_chart[c_idx], dwg)
                # Final '[1:]' shaves the '#' prefix off RGB color descript.
                splotch_grp = \
                        dwg.g(id=f'splotch_{idx}_{(color_chart[c_idx])[1:]}')
                splotch_grp.add(splotch)
                dwg.defs.add(splotch_grp)
        for idx in range(len(TREE_PATHS)):
            crown = self.base_tree(idx, TREE_PATHS[idx],
                    BASE_TREE_COLOR, dwg)
            dwg.defs.add(crown)

    def barren_branch(self, start_pt, angle, br_len, thickness,
                      level, tr_grp, dwg):
        """Recursively extend barren branches."""
        if level != 0:
            # Divide into two or three branches.
            branch_count = self.rng.integers(2)
            if branch_count == 0:
                angle_list = [1/6, 0, -1/6]
            else:
                angle_list = [1/12, -1/12]
            for angle_increm in angle_list:
                new_angle = angle + (2 * math.pi * angle_increm)
                end_pt = calc_arc_pt(start_pt, br_len, new_angle)
                branch = dwg.line(tuple(start_pt), tuple(end_pt))
                branch.stroke(BARREN_TREE_COLOR, width=thickness)
                tr_grp.add(branch)
                self.barren_branch(end_pt, new_angle, br_len,
                        round(thickness * 0.80, 3), level -1, tr_grp, dwg)
        else:
            # End branch with a point.
            perp_angle = perpendicular_angle(angle)
            left_offset = calc_arc_pt(start_pt, (thickness/2), perp_angle)
            right_offset = calc_arc_pt(start_pt, -(thickness/2), perp_angle)
            tip_pt = calc_arc_pt(start_pt, 7, angle)
            br_tip = begin_move_to_path(start_pt, (thickness/2), 
                            perp_angle, dwg)
            br_tip.push(define_line(tip_pt))
            br_tip.push(line_to(start_pt, -(thickness/2), perp_angle))
            br_tip.push("Z")
            br_tip.fill(BARREN_TREE_COLOR)
            tr_grp.add(br_tip)

    def barren_tree(self, tr_radius, idx, dwg):
        """Draw a barren, dried up tree."""
        TRUNK_PRCTG = 0.15
        FIRST_SPLIT = 0.3
        LEVELS = 2
        ctr_pt = [0,0]
        tree_url = f'barren_tree_{tr_radius}_{idx}'
        tree_grp = dwg.g(id=tree_url)
        length = tr_radius * FIRST_SPLIT
        thickness = min(tr_radius * TRUNK_PRCTG, 5)
        trunk = dwg.circle(tuple([0,0]), thickness, fill=BARREN_TREE_COLOR)
        tree_grp.add(trunk)
        for j in range(5):
            angle = j * (2 * math.pi) / 5
            branch_nuckle = calc_arc_pt(ctr_pt, length, angle)
            branch = dwg.line(tuple(ctr_pt), tuple(branch_nuckle))
            branch.stroke(BARREN_TREE_COLOR, width=thickness,
                        linecap='round', opacity=1)
            tree_grp.add(branch)
            self.barren_branch(branch_nuckle, angle,
                    round(length * 0.9, 3),
                    round(thickness * 0.85, 3), LEVELS, tree_grp, dwg)
        dwg.defs.add(tree_grp)


    def make_conifer_grad(self,tr_radius, light_color, dark_color, dwg):
        """Gradient for conifer style 'trees'."""
        grad = dwg.radialGradient(id=f'conifer_gradient_{tr_radius}',
                    r=tr_radius, center=([0,0]),
                    gradientUnits="userSpaceOnUse", opacity=1)
        grad.add_stop_color(offset="1%", color=light_color)
        grad.add_stop_color(offset="100%", color=dark_color)
        dwg.defs.add(grad)

    def make_conifer(self, tr_radius, dwg):
        """Circles with a green gradient and chevrons as branches."""
        tree_url = f'conifer_{tr_radius}'
        grad_url = f'url(#conifer_gradient_{tr_radius})'
        tree_grp = dwg.g(id=tree_url)
        conifer = dwg.circle(r=tr_radius, fill=grad_url)
        tree_grp.add(conifer)
        chev_nm = f'chevron_len_{self.chev_len}_ofs_{self.chev_offset}'
        conifer_radii = [tr_radius / 6, tr_radius / 2,
                tr_radius + 2 - self.chev_len]
        for idx in range(len(conifer_radii)):
            flag = False
            if idx % 2 == 1:
                # Offset starting sweep for next loop so chevrons 
                # are not aligned in a row.
                flag = True
            chev_loop = self.circle_of_chevrons([0,0],
                    round(conifer_radii[idx], 3), chev_nm, flag, dwg)
            tree_grp.add(chev_loop)
        dwg.defs.add(tree_grp)

    def define_chevron(self, offset, length, color, dwg):
        """Draw a chevron (^) of specified size and color.
        offset is from centerline to arm-ends."""
        ctr_segment = [[0,0],[length,0]]
        lower_pt = calc_arc_pt([0,0], -offset, math.pi / 2)
        chevron = dwg.g(id=f'chevron_len_{length}_ofs_{offset}')
        chev_arms = begin_move_to_path([0,0], offset, math.pi / 2, dwg)
        chev_arms.push(define_line(ctr_segment[1]))
        chev_arms.push(define_line(lower_pt))
        chevron.add(chev_arms)
        chevron.stroke(color, width=1, opacity=1)
        chevron.fill(None, opacity=0)
        dwg.defs.add(chevron)

    def get_chevron_len_offset(self, chev_nm):
        """Parse chevron length and offset from defs ID."""
        len_start = chev_nm.find('len_') + 4
        len_end = int(chev_nm.find('_ofs_'))
        offset_start = int(len_end)+ 5
        length = int(chev_nm[len_start:len_end])
        offset = int(chev_nm[offset_start:])
        return tuple([length, offset])


    def circle_of_chevrons(self, ctr_pt, radius, chev_nm, offset_flag, dwg):
        """Create a loop of chevrons pointed outward with openings.
        """
        chev_grp = dwg.g()
        chev_len, chev_offset = self.get_chevron_len_offset(chev_nm)
        chev_width = 1 + chev_offset * 2
        denom = arc_lens_in_half_circle(radius, chev_width)
        # offset and flag so chevrons don't have appear in a row.
        offset = 0
        if offset_flag:
            offset = 1 / denom * 2
        for num in range(denom * 2):
            num = num + offset
            chev = dwg.use(href=f'#{chev_nm}')
            place_pt = calc_arc_pt(ctr_pt, radius, math.pi * num / denom)
            chev.translate(place_pt)
            chev.rotate(round(math.degrees(math.pi * num / denom), 3))
            chev_grp.add(chev)
        return chev_grp

    def plant_trees(self, t_data, side, tile_cmds, dwg):
        """Place trees per tile definition."""
        DENOM = 8
        if "BarrenTrees" in t_data[side].keys():
            idx = 0
            for tree in t_data[side]["BarrenTrees"]:
                tree_nm = f'{tree["Name"]}_{idx}'
                offset = transform_pt_def(tree)
                numerator = self.rng.integers(DENOM)
                angle = math.degrees(math.pi * numerator / DENOM)
                tile_cmds.add(place_defs_element(
                                    tree_nm, offset, angle, dwg))
                # alternate index to get a little variation.
                idx = (idx + 1) % 2
        for tree_kind in ["ConiferTrees", "BaseTrees"]:
            if tree_kind in t_data[side].keys():
                for tree in t_data[side][tree_kind]:
                    tree_nm = f'{tree["Name"]}'
                    offset = transform_pt_def(tree)
                    numerator = self.rng.integers(DENOM)
                    angle = math.degrees(math.pi * numerator / DENOM)
                    tile_cmds.add(place_defs_element(
                            tree_nm, offset, angle, dwg))
        return tile_cmds



# Data defining Base Trees (the green polygons spotted with sundry ellipses).
# Each element defines the set of color splotches for the
# canopy of the matching tree-canopy outline (by index) in TREE_PATHS.
# index is tree number,
# tuples are (tree-lipse index, center offset, splotch shade index,
# rotation in degrees)
BASE_TREE_DEFS = [
    [
#        (TREE_LIPSE idx,               canopy offset[x,y],
#         splotch-color idx,            rotation angle)
        (0, [-7,-6], 5, 0),
        (3, [-3,-13], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (0, [5,-11], 8, 0),
        (1, [-6,10], 8, -45),
        (1, [1,-5], 6, 0),
        (1, [14,-1], 4, 0),
        (3, [2,12], 6, 0),
        (1, [8,11], 4, 0),
        (2, [-14,-2], 8, 0),
        (1, [-12,6], 5, 0)
    ],
    [
        (1, [-8, -8], 6, 0),
        (1, [-2, -9], 5, 0),
        (2, [6, -7], 8, 0),
        (1, [-10, -2], 5, 0),
        (3, [-3, -3], 8, 0),
        (1, [9, -2], 5, 0),
        (1, [3, 0], 6, 0),
        (1, [-7, 5], 5, 0),
        (1, [-1, 6], 6, 0),
        (3, [4, 9], 8, 0),
        (2, [8, 4], 6, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
    ],
    [
        (0, [-7,-8], 8, 0),
        (3, [0,-4], 5, 0),
        (0, [-1,9], 7, 90),
        (2, [7,8], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 7, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [1,10], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 5, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (0, [6,-10], 8, 0),
        (2, [-14,-1], 8, 0),
        (2, [-8,9], 8, -45),
        (3, [13,0], 8, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [0,-8], 8, 0),
        (1, [-2,7], 5, 0),
        (2, [9,6], 8, 45),
        (3, [-5,1], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (3, [-12,0], 5, 0),
        (1, [-8,9], 5, 0),
        (1, [4,10], 5, 0),
    ],
    [
        (3, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (0, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (1, [3,-9], 8, 0),
    ],
    [
        (3, [-7,-6], 5, 0),
        (3, [0,-4], 8, 0),
        (1, [-2,9], 5, 90),
        (2, [9,6], 8, 45),
        (0, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (1, [3,-9], 4, 0),
        (1, [3,9], 8, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [-3,-13], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (0, [6,-13], 8, 0),
        (2, [-18,-10], 8, 30),
        (2, [-8,12], 8, -45),
        (1, [1,-5], 6, 0),
        (1, [17,-3], 4, 0),
        (1, [18,5], 5, 0),
        (3, [0,15], 6, 0),
        (1, [10,13], 4, 0),
        (4, [-16,-2], 4, 0),
        (1, [-17,6], 5, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [11,-4], 4, 0),
        (0, [6,-10], 8, 0),
        (2, [-14,-1], 8, 0),
        (2, [-8,10], 8, -45),
        (1, [3,11], 4, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [-2,-11], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [0,-2], 5, 0),
        (4, [8,-4], 5, 0),
        (0, [6,-10], 8, 0),
        (2, [-14,-1], 8, 0),
        (2, [-8,9], 8, -45),
        (1, [-3,14], 6, 0),
        (1, [5,13], 5, 0),
    ],
    [
        (3, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [0,7], 5, 90),
        (2, [9,6], 8, 45),
        (0, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [-2,7], 5, 0),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 8, 0),
        (3, [2,-11], 8, 0),
    ],
    [
        (0, [-7,-6], 5, 0),
        (3, [0,-4], 5, 0),
        (1, [-2,7], 5, 90),
        (2, [9,6], 8, 45),
        (3, [-7,3], 8, 0),
        (3, [3,2], 5, 0),
        (4, [8,-4], 5, 0),
        (3, [2,-11], 8, 0),
        (3, [3,11], 8, 0),
    ],
    [ ],
]

# Splotch shapes (ellipses) for base trees.
TREE_LIPSES = [
# ellipse rx, ry
{3.18, 2.86},
{1.32, 1.57},
{2.7, 1.79},
{1.6, 2.05},
{1.66, 2.63},
{4.19, 1.87},
{0.848, 5.17},
]

# (Too many) splotch color options.
SPLOTCH_COLORS = [
    "#24d000",      # light greens
    "#18cc00",
    "#00c800",
    "#00b800",
    "#00a800",
    "#009000",
    "#008000",      # dark greens
    "#007000",
    "#006000",
    "#005000",
]

# Base Tree polygon outlines (individual tree crown outlines).
TREE_PATHS = [
"m4,-18 l5.9,3.27 4.5,4.35 2.2,6.6 1,8.3 -2,7.1 -7.5,5.1 -8.8,0.5 -7.6,-2.9 -6.6,-5.1 -3,-7.5 0.3,-8.4 4.2,-5.7 5.6,-2.72 5.3,-2.3 z",
"m-15,-3 l2.4,-4.7 2.6,-2.4 4.8,-2.8 5.4,-0.3 7,1.5 5,4.9 3,7.2 v 6 l -4,4.8 -7,2.6 -6,-0.5 -7.1,-3.4 -4.5,-4.8 z",
"m-13,-9 l6,-2.54 7,-0.8 5,1.64 6,2.92 4,5.62 1,6.7 -3,5 -5,3.1 -6,1.3 -9,-0.8 -7,-4.7 -2,-5.7 c 0,0 -1,-5.2 0,-7.3 1,-2.16 3,-4.44 3,-4.44 z",
"m-6,-15 l-2,1.69 -3,4 -2,5.2 2,6.5 0,4.7 1,4.5 5,3.8 5,-0.3 6,-1.9 5,-3.4 -1,-7.9 3,-5.7 -3,-8 -5,-1 -5,0.1 z",
"m-13,-9 l3.9,-2.7 4.4,-1.8 5.4,-1.3 6.6,-0.7 4.5,2 6.1,5.3 1.1,7.6 -1.6,6.6 -4.5,6.3 -7.4,2.4 -5.9,0.6 h -9.1 l -3.6,-3.5 -4.3,-4.2 -0.8,-5.3 -1.34,-6.2 z",
"m-11,-13 l-2.2,4.8 -4.7,5.6 1.6,6.1 2.6,4.8 4,4.7 6.4,2.4 4.7,-0.8 6.6,0.6 3,-2.4 3.9,-6.9 0.4,-5.3 -2.5,-4.1 -1.9,-4 -2.1,-2.9 -4.5,-2.6 -6.6,-1 z",
"m-14,0 l2,-4.6 5,-5.5 5,-2.5 4,-0.8 5,1 5,4.7 2,5.7 v 5.7 l -3,4.3 -4,3.2 -6,1.8 -8,-0.8 -6,-4.2 z",
"m-13,-6 l4,-3.8 6,-2.36 h 7 l 4,2.09 5,6.17 v 6.3 l -1,8 -5,3 -9,1 -6,-1.3 -4,-4.8 -4,-4.9 z",
"m-12,-15 l5.4,-2.3 6.6,-1.6 7.4,-0.6 7.7,4.3 5,6.1 4.5,10.3 -1.1,4 -4.7,6.9 -5.3,5.8 -7.2,-0.3 -7.7,1.9 -6.6,-0.5 -6.4,-2.7 -5.94,-7.7 -4.56,-7.9 0.77,-8.8 3.9,-6.9 z",
"m-16,-6 l2.1,-4.8 6.1,-3.7 4.5,-1.9 5.8,1.1 6.1,1.1 4.5,3.7 5.2,7.7 1,9.8 -3,3.1 -6.3,5.9 -5.3,-0.8 -7.7,2.6 -6.9,0.8 -4.5,-3.7 -2.4,-6.3 -3.2,-3.8 2.1,-6.1 z",
"m-12,-12 l5,-5 6,-1 8,0.5 6,5 4,6.4 2,7.4 v 8.5 l -4,4.5 -7,4.5 -7,1 -9,-0.5 -4,-3.7 -5,-6.1 -3,-8.2 v -5.6 l 1,-5.8 z",
"m-12,-5 l4,-4.5 5,-1.8 6,-1.1 5,1.6 4,4 3,9.3 -4,7.1 -7,4.3 -7,-1.4 -8,-4.7 -2,-5.3 z",
"m-11,-10 l5.2,-3 6.1,-2 h 6.1 l 6.6,3 2.9,7 1.4,7 -1.6,6 -4,4 -8.7,3 h -8.1 l -6.9,-4 -3.5,-6 -0.6,-8 z",
"m0,-15 l5,1 5.3,5 2.1,4 1.9,9 -1.9,6 -5.5,5 -5.1,1 -7.4,-1 -6.1,-4 -3.4,-7 -0.3,-7 2.9,-6 4.5,-5 z"
]




