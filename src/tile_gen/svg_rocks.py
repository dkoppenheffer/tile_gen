# copyright 2023 Daniel P. Koppenheffer
""" mr_svg_rocks - Rocks class.
Draw and distribute pebbles, rocks, boulders and rectangular blocks."""

import math

#import svgwrite

from tile_gen.geom_utils import \
    calc_arc_pt, angle_two_pts, perpendicular_angle, \
    calc_arc_sweep, calc_sweep_dist, calc_polar_prctg_pt

from tile_gen.misc_draw import \
    emplace_element, place_defs_element

from tile_gen.svg_caves import \
    TUNNEL_BACKGROUND
   
#################################
# Rock, boulder, pebble constants.
# Rock colors "#babd86" "#b8b8b8" "#d3d7cf" "#888a85" "#8a9292" "8a8888"
ROCK_COLOR = "#8a8888"
BOULDER_SHADOW_COLOR = "#595959"
ROCK_SHADOW_COLOR = '#242424'
PATH_DEBRIS_HILITE = "#b48800"
TUNNEL_DEBRIS_HILITE = "#888888"
# Other rock data tables at end of file.

class Rocks:
    """Rock images for landscape interest.

    Also used at foot of mountains as fallen rock debris.
    Rock shapes and details were manually drawn.
    rock kind is either "rock" or "boulder". They map directly to
    table pairs.
    pebbles are a subset of 'rocks'.
    Debris are another subset of 'rocks' appearing in two differing
    colorations: "tunnel_debris" (rock-gray) and "path_debris"
    (gold-tan).

    """
    def __init__(self, rand_ng):
        self.rng = rand_ng      # numpy random number generator

    #r_kind (rock kind) is "rock" or "boulder".

    def make_rock(self, r_idx, r_kind, r_data, r_shadow_d, rock_color, dwg):
        """Create a rock in the dwg.defs."""
        rock_url = f'{r_kind}_{r_idx}'
        rock_grp = dwg.g(id=rock_url)
        rock_path = dwg.path(d=("m", 0, 0))
        rock_path.push(f' {r_data}')
        rock_path.stroke(TUNNEL_BACKGROUND, width=1)
        rock_path.fill(rock_color, opacity=1)
        rock_grp.add(rock_path)
        # Shadow is the heavy outline on one side of a rock.
        shadow_path = dwg.path(d=("m", 0, 0))
        shadow_path.push(f' {r_shadow_d}')
        shadow_path.stroke(ROCK_SHADOW_COLOR, width=2, opacity=1)
        shadow_path.fill(opacity=0)
        rock_grp.add(shadow_path)
        return rock_grp

    def make_boulder(self, r_idx, r_kind, r_data, shade_d, \
                      bkgd_color, shade_color, dwg):
        """Create a boulder in the dwg.defs."""
        rock_url = f'{r_kind}_{r_idx}'
        rock_grp = dwg.g(id=rock_url)
        rock_path = dwg.path(d=("m", 0, 0))
        rock_path.push(f' {r_data}')
        rock_path.stroke(TUNNEL_BACKGROUND, width=1)
        rock_path.fill(bkgd_color, opacity=1)
        rock_grp.add(rock_path)
        # Shade is the darker gray over a portion of the rock's surface.
        # This applies only to "boulders" (large rocks) and is in lieu
        # of shadow.
        shade_path = dwg.path(d=("m", 0, 0))
        shade_path.push(f' {shade_d}')
        shade_path.stroke(shade_color, width=2, opacity=1)
        shade_path.fill(shade_color, opacity=1)
        rock_grp.add(shade_path)
        return rock_grp

    def string_of_pebbles(self, dwg):
        """Create string-line of 2-4 pebbles."""
        pebbles = dwg.g()
        count = self.rng.integers(2,5)
        base_offset = 3
        for j in range(count):
            peb_idx = self.rng.integers(len(PEBBLES))
            rock_idx = PEBBLES[peb_idx]
            peb_url = f'#rock_{str(rock_idx)}'
            chip = dwg.use(href=peb_url)
            chip.translate(6 * (j + 1) + base_offset, 0)
            pebbles.add(chip)
        return pebbles

    def get_debris(self, debris_kind, coord, dwg):
        """ Return debris with appropriate coloration.

        Debris are small to medium size rocks"""
        debris_idx = self.rng.integers(len(DEBRIS))
        idx = DEBRIS[debris_idx]
        rock_url = f'{debris_kind}_{idx}'
        # Rotate to one of eight directions.
        idx = self.rng.integers(8)
        angle = round(45 * idx, 3)
        rock_found = place_defs_element(rock_url, coord, angle, dwg)
        return rock_found

    def fallen_rocks(self, rock_loc, dwg):
        """Place a medium rock with possibility of string of pebbles.
        """
        #r_idx = self.rng.integers(len(ROCK_OUTLINES))
        # 2-12: indices; skipping pebbles within regular rock defs.
        r_idx = self.rng.integers(2, 12)
        rock_nm = f'rock_{str(r_idx)}'
        eighth = self.rng.integers(8)
        angle = round(math.degrees(math.pi * eighth / 8), 3)
        return place_defs_element(rock_nm, rock_loc, angle, dwg)

    def sprinkle_boulders(self, count, dwg):
        """Randomly place boulders in a broad belt around tile center.

        Belt covers from r * 35% to r * 70%.  To be replaced
        or rewritten. Random numbers sans "supervision" results in
        bunching and-or placement beneath later appearing elements esp.
        clearings and paths."""
        boulders = dwg.g()
        DENOM = 12
        INNER_BELT = 0.35
        BELT_NOTCH = 0.05
        polar_pos = {
            "RadiusPrctg": 0,
            "Numerator": 0,
            "Denominator": DENOM,
        }
        for j in range(int(count)):
            # Radius factor
            b_notches = self.rng.integers(8)
            r_factor = INNER_BELT + (b_notches * BELT_NOTCH)
            # Boulder choice
            b_idx = self.rng.integers(len(LARGE_ROCKS))
            new_rock = dwg.use(href=f'#boulder_{b_idx}')
            # Polar angle
            numerator = self.rng.integers(24)
            polar_pos["RadiusPrctg"] = r_factor
            polar_pos["Numerator"] = numerator
            r_pt = calc_polar_prctg_pt(polar_pos, [0,0])
            rot_num = self.rng.integers(24)
            angle = round(math.degrees(math.pi * rot_num / DENOM), 3)
            emplace_element(new_rock, r_pt, angle, boulders)
        return boulders

    def draw_rand_boulders(self, t_data, side, tile_cmds, dwg):
        """Randomly draw 'count' boulders across the tile.

        Unfortunately, many boulders are drawn where they're overwritten by
        later tile elements. Lacks any kind of clipping or 'obstacle'
        avoidance.
        """
        if "RandomBoulders" in t_data[side].keys():
            count = t_data[side]["RandomBoulders"]["Count"]
            tile_cmds.add(self.sprinkle_boulders(count, dwg))

    def define_debris(self, debris_kind, debris_hilite, dwg):
        """Create debris elements in dwg.defs.

        There are two kinds: 'tunnel_debris' and 'path_debris'."""
        for d_idx in range(len(DEBRIS)):
            idx = DEBRIS[d_idx]
            a_rock = self.make_rock(idx, debris_kind,
                    ROCK_OUTLINES[idx], ROCK_SHADOW[idx],
                    debris_hilite, dwg)
            dwg.defs.add(a_rock)

    def debris_factory(self, dwg):
        """Define debris elements.

        Two colors of debris: tan-gold for hidden paths and gray for
        secret passages and bordering vales (valleys).
        """
        self.define_debris('tunnel_debris', TUNNEL_DEBRIS_HILITE, dwg)
        self.define_debris('path_debris', PATH_DEBRIS_HILITE, dwg)

    def rock_factory(self, dwg):
        """Generate rocks, boulders and debris into dwg.defs."""
        for idx in range(len(ROCK_OUTLINES)):
            a_rock = self.make_rock(idx, "rock",
                    ROCK_OUTLINES[idx], ROCK_SHADOW[idx],
                    ROCK_COLOR, dwg)
            dwg.defs.add(a_rock)
        for idx in range(len(LARGE_ROCKS)):
            a_rock = self.make_boulder(idx, "boulder",
                    LARGE_ROCKS[idx], LARGE_ROCK_SHADE[idx],
                    ROCK_COLOR, BOULDER_SHADOW_COLOR, dwg)
            dwg.defs.add(a_rock)

    def straight_path_debris(self, seg, width, min_dense, max_dense,
            debris_kind, dwg):
        """Lay down debris along a straight path.

        Debris are 'pebbles' from the Rocks class.
        Width is horizontal range for random placement.
        Min_ and Max_dense (density) are the 'vertical' window for random
        placement.
        """
        max_dist = math.dist(seg[0], seg[1])
        breadth_lim = round((width / 2) -4)
        debris_grp = dwg.g()
        cvrd_dist = self.rng.integers(min_dense, max_dense)
        angle = angle_two_pts(seg[0], seg[1])
        perp_angle = perpendicular_angle(angle)
        while cvrd_dist < max_dist:
            # What offset from centerline for next object placement.
            horz_offset = self.rng.integers(-breadth_lim, breadth_lim + 1)
            vert_pt = calc_arc_pt(seg[0], cvrd_dist, angle)
            drop_spot = calc_arc_pt(vert_pt, horz_offset, perp_angle)
            debris = self.get_debris(debris_kind, drop_spot, dwg)
            debris_grp.add(debris)
            cvrd_dist += self.rng.integers(min_dense, max_dense)
        return debris_grp

    def curved_path_debris(self, swp_start, swp_end, ctr_pt, radius, width,
                       min_dense, max_dense, debris_kind, dwg):
        """Lay down debris along a semi-circular path."""
        swp_total = calc_arc_sweep(swp_start, swp_end)
        total_dist = calc_sweep_dist(radius, swp_start, swp_end)
        breadth_lim = round((width / 2) -4)
        debris_grp = dwg.g()

        cvrd_dist = self.rng.integers(min_dense, max_dense)
        while cvrd_dist < total_dist:
            cvrd_prctg = round(cvrd_dist / total_dist, 3)
            angle = swp_start + (swp_total * cvrd_prctg)
            ctrln_pt = calc_arc_pt(ctr_pt, radius, angle)
            horz_offset = self.rng.integers(-breadth_lim, breadth_lim + 1)
            drop_spot = calc_arc_pt(ctrln_pt, horz_offset, angle)
            debris_grp.add(self.get_debris(debris_kind, drop_spot, dwg))

            cvrd_dist += self.rng.integers(min_dense, max_dense)
        return debris_grp



# Rock polygon paths.  Medium and small (pebble sized).
# I ought to have segregated pebbles in a separate table.
# Rock outlines and their shadows have synchronized indices.
ROCK_OUTLINES = [
"m -2,-1 l2.8,-1.7 1.5,0.9 0.1,2.3 -1.8,1.8 -1.5,0.1 -1.6,-0.4 z",
"m -3,0 l1.6,-1.8 2.6,-0.8 2.7,1.5 -0.3,2.4 -1.3,1.7 -2.7,0.7 -2.4,-0.8 z",
"m -7,-3 l6,-1.9 8,0.4 2,4.9 -1,4.2 -5,3.6 -6,0.3 -5.1,-3.8 -0.5,-4.3 z",
"m -6,-2 l5,-3.2 7,0.2 4,4.6 -2,4.8 -5,2.8 -6,-0.2 -4,-2.7 -2,-1.2 -1,-3 z",
"m -7,0 l3,-4.2 7,-2.4 3,2.9 1,6.6 -4,4.4 h -5 l -4,-2.5 z",
"m -5,0 l1,-4.6 3,-1.77 5,0.64 2,3.73 -1,5.4 -5,2.3 -5,-0.8 z",
"m -7,-2 l2.6,-3 3.5,-1.9 h 2.9 l 4.5,1.5 2.1,3.6 0.3,2.9 -1.6,4.5 -3.2,1.8 -5.8,0.2 -4,-1.3 -2.9,-2.8 -0.5,-3.7 z",
"m -8,-2 l2.6,-3 3.5,-1.9 h 2.9 l 4.5,1.5 2.1,3.6 0.3,2.9 -1.6,4.5 -3.2,1.8 -5.8,0.2 -4,-1.3 -2.9,-2.8 -0.5,-3.7 z",
"m -6,-2 l4.41,-2.9 4.8,1 2.1,4 -1.6,3.7 -2.9,1.1 -4.7,-0.3 -2.74,-2.1 z",
"m -4,-2 l3.4,-2.4 2.7,0.3 2.9,2.6 0.2,3.7 -0.8,2.4 -1.8,2.2 -3.5,1.8 -2.3,-0.3 -1.6,-2.3 -0.8,-3.5 z",
"m -3,-4 l-1.9,2.4 0.3,3.4 1.1,1.9 3.1,1.1 4,-1.9 0.5,-4 -2.6,-4.2 z",
"m -5,-2 l3.2,-2.9 2.1,0.8 2.4,-0.3 1.3,1.1 0.3,2.9 -1.3,3.2 -3.5,1.8 h -3.7 l -1.6,-2.9 z",
"m -5,-2 l-1.1,3 2.1,2.6 2,0.5 4,-0.5 1,-3.4 -1,-4 -3,-0.8 z",
"m -2,-4 l-2,2.6 1,3.5 2,1 3,-1.3 1,-3.4 -1,-2.9 z",
"m -3,-3 l-2,3.4 1,1.9 3,1.3 3,-1 1,-2.7 v -2.9 l -2,-1.3 z",
]

ROCK_SHADOW = [
"m 2,-1 l-0.1,2.4 -2,1.9 -2.4,-1 v 0",
"m -3,3 l2.1,1.1 3,-0.7 1,-1.6 0.3,-2.3",
"m -8,5 l5,3.9 6,-0.2 5,-3.8 1,-4.2",
"m -9,4 l6,4.1 6,0.2 5,-2.9 2,-5.2",
"m -6,5 l4,2.3 h 6 l 3,-4.7 -1,-6.2",
"m -5,5 l6,1.3 4,-2.4 1,-5.1",
"m -9,4 l2.7,2.6 3.9,1.4 6.1,-0.1 3.5,-1.9 1.6,-4.6",
"m -9,3 l5,3.9 6,-0.2 5,-3.8 1,-4.2",
"m -7,3 l3.26,2.4 4.1,0.3 3.3,-1.3 1.8,-4.3",
"m -5,5 l1.6,2.4 2.9,0.3 3.2,-1.9 2.1,-2.6 0.3,-2.4 0.2,-3.2",
"m -5,2 l1.4,2.1 3.4,1.3 4,-2.1 0.2,-4.2",
"m -6,2 l1.6,2.9 4,0.3 3.2,-1.9 1.3,-3.4 v -2.9",
"m -6,1 l2.1,2.9 2,0.5 4,-0.5 1,-3.2 -1,-4.2",
"m -4,-1 l1,3.4 2,1.1 3,-1.3 1,-3.7",
"m -5,1 l1,1.9 3,1.3 3,-1.3 1,-2.1 v -2.9",
]

# Large Rocks are 'boulders' in code.
# Rock Rocks and their shade have synchronized indices.
LARGE_ROCKS = [
"m -10,-6 l5.3,-3.2 7.2,-0.74 6.3,1.54 2.7,4.2 v 6.7 l -3.7,4.1 -6.1,2 h -6.4 l -6.1,-3.6 -2.4,-5.1 z",
"m -14,-6 c -2.22,4.7 -1.96,4.7 -1.96,4.7 l 1.62,6.1 3.82,4 6.5,2.4 7.6,-0.8 7.1,-4.8 2.1,-5.8 -1.8,-6.9 -7.4,-4.8 -4.6,1.9 -3.2,-1.3 -4.3,1.3 z",
"m -8,-9 -4,3.2 -0.8,3.7 -3.7,4.3 1.8,3.9 v 5.1 l 5.6,2.9 7.2,0.5 6.6,-1.3 4,-3.5 2.6,-6.1 0.8,-7.4 -2.1,-7.1 -6.1,-2.9 -6.1,-0.8 -1.9,2.9 z",
"m -8,-7 -2.1,3.7 -0.3,5 0.8,4.3 v 3.1 l 4,2.4 6.1,-0.2 7,1.8 5,-3.4 2,-4.3 -3,-3.7 -1,-4.2 -4,-4.3 -5,0.6 -4,-2.7 -3.4,-0.5 z",
"m -8,-8 -2,3.4 -3,1.4 -1,2.6 1,3.7 v 2.4 l 3,4.2 1,5.1 9,2.4 7,-1.9 4,-2.9 v -6.9 l 3,-6.1 -2,-4.8 -5,-2.9 -4,-3.7 -5,0.3 z",
"m -6,-11 5,-0.6 3,-3.1 6,0.5 4,3.7 1,5.8 3,6.1 -1,6.9 -4,4 -7,1 -5,4.6 -8,-0.3 -7,-3.2 -4,-8.2 4,-8.5 1,-6.9 z"
]

LARGE_ROCK_SHADE = [
"m -11,4 5.9,3.5 h 6.1 l 6.1,-1.9 3.7,-4.2 v -6.4 l -2.7,0.5 -1,1.5 -2.7,0.6 -1.3,5 -0.8,0.1 -1.1,1.2 -3.7,-0.8 -4,-2.9 -2.9,-0.2 -1.8,1.4 z",
"m -16,-2 1.44,5.5 4.03,3.7 6.4,2.7 7.7,-0.5 6.9,-5.1 1.8,-5.5 -5.8,-4.5 -5.7,3.9 -4.8,-1.5 -4.6,-0.8 z",
"m -14,11 4.7,2.3 7.2,0.8 6.9,-1.3 4,-3.2 2.4,-6.1 1,-7.4 -2.4,-7.1 -1.8,1 -0.3,5.1 -4.8,0.5 -2.9,2.4 -2.4,0.5 -0.8,4.2 -1.8,1.6 -1.6,4.3 -5,-0.3 z",
"m -10,-3 5.3,3.7 6,-1.6 3,3.7 1,3.5 7,3.9 -5,3.2 -7,-1.8 -6,0.2 -3.8,-2.4 0.3,-3.1 -1.4,-4.5 z",
"m -10,14 9,2.7 7,-1.9 4,-2.6 v -6.9 l -6,-1.1 -3,1.6 -5,-4 -4,1.9 -3,5.5 z",
"m -16,-1 1,-7.2 9,-1.9 5,-0.5 3,-3.4 6,0.8 4,3.7 1,5.3 -3,3.9 -7,-1.3 h -5 l -6,2.9 -4,-2.6 z"
]

# Indices into ROCK_OUTLINES for smaller rocks and for pebbles.
DEBRIS = [0, 1, 10, 11, 12, 13, 14]
PEBBLES = [0, 1]

