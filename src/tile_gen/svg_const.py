# copyright 2023 Daniel P. Koppenheffer
"""Constants for mr_svg_tile.py"""

TILES_PATH = "./tiles"
SVG_PATH = "./svg"
JSON_SFX = ".json"

GB_FONT = "Liberation Sans, Bold"
FONT_SIZE = "26"
LABEL_STYLE = 'font-size: 140%; font-family: sans; font-weight: bold'
CLRING_STYLE = 'fill: yellow; stroke: yellow; ' \
    + LABEL_STYLE
CLEARING_RADIUS = 50
TILE_RADIUS = 300
PATH_WIDTH = 36
HID_PATH_WIDTH = PATH_WIDTH - 10
VALE_FACTOR = 2.25

# Hexagon: corner and side mid-point coordinates.
# Pre-computed for a 300 radius (measured to corner) hexagon.
# For radius 300; start at 9 Oclock, move clockwise.
HEX_CORNERS = [
    [-300,0], [-150.0,-259.81], [150.0,-259.81],
    [300,0], [150.0,259.81], [-150.0,259.81],
]

# For radius 300; start at 12 Oclock, move clockwise.
HEX_MIDPOINTS = [
    [], [0.0, -259.81], [225.0, -129.905], [225.0, 129.905],
    [0.0, 259.81], [-225.0, 129.905], [-225.0, -129.905]
]

HEX_EDGES = [
    [HEX_CORNERS[0], HEX_CORNERS[1]],
    [HEX_CORNERS[1], HEX_CORNERS[2]],
    [HEX_CORNERS[2], HEX_CORNERS[3]],
    [HEX_CORNERS[3], HEX_CORNERS[4]],
    [HEX_CORNERS[4], HEX_CORNERS[5]],
    [HEX_CORNERS[5], HEX_CORNERS[0]]
]

# zeroeth element unused.
CLEARING_PTS = [[], [], [], [], [], [], []]


##########################
# COLORS
# Outdoor (surface) paths
PATH_FOREGROUND = "#743601"
PATH_BACKGROUND = "#957000"

# Hidden path (surface)
HIDDEN_BACKGROUND = "#542a0c"
HIDDEN_FOREGROUND =  "darkslategray"

# Tile overall background colors
WOODS_BACKGROUND = "#006400"
VALE_BACKGROUND = "#005200"

#Color Magic backgrounds
PURPLE_MAGIC_BKGD = "#440055"
PURPLE_MAGIC_FRGD = "#9932cc"
GOLD_MAGIC_BKGD = "#7F530A"
GOLD_MAGIC_FRGD = "#e8cc4c"
GREY_MAGIC_BKGD = "#335631"
GREY_MAGIC_FRGD = "#606060"
GREY_MAGIC_VALE = "#283527"  # "#344632"
BRIDGE_BACKGROUND = "black"
BRIDGE_FOREGROUND = "#373737"   # Slat color

TILE_LABEL_COLOR = "yellow"
ALT_LABEL_COLOR = "#005000"

# Used to draw mountain clipping paths instead of mntn clearings.
# Index starts at zero and excludes non-mntn clearings from consideration.
SKIP_INDICES = []
#SKIP_INDICES = [0, 2, 3, 4, 5]
#SKIP_INDICES = [0, 1, 2, 3, 4]
#SKIP_INDICES = [0, 1, 2, 3, 5]


