# copyright 2023 Daniel P. Koppenheffer

"""Help tool for discovering where path end points belong.

When setting up complex tile path geometry, path end points may not appear
where intended or may not draw at all. This tool over lays red dots showing
where path end points were intended to be located. This may help with
getting paths to connect or even to draw when not appearing.
"""

import svgwrite

from tile_gen.geom_utils import \
    find_circle

from tile_gen.unpk_data import \
    calc_three_pt_arc_values

from tile_gen.misc_draw import \
    calc_segment, transform_pt_def

def draw_red_dot(point, dwg):
    circle = dwg.circle(tuple(point), 3)
    circle.fill('red').stroke('red', width=1, opacity=1)
    return circle

def mark_arc_points(path_d, red_dot_grp, dwg):
    """Draws red dots at curved path calculated end and center points."""
    if path_d["PathDataCfg"] == "PolarPrctgWithPts":
        ctr_pt = transform_pt_def(path_d["CenterPt"])
        cart_segm = calc_segment(path_d["StartPt"], path_d["EndPt"])
        for pt in [ctr_pt, cart_segm[0], cart_segm[1]]:
            red_dot_grp.add(draw_red_dot(pt, dwg))

    elif path_d["PathDataCfg"] == "ThreePoint":
        arc_d = calc_three_pt_arc_values(path_d)
        h_k_r = find_circle(arc_d["StartPt"], arc_d["MidPt"], arc_d["EndPt"])
        radius = round(h_k_r[2], 3)
        ctr_pt = [round(h_k_r[0], 3), round(h_k_r[1], 3)]
        for pt in [ctr_pt, h_k_r[0], h_k_r[1]]:
            red_dot_grp.add(draw_red_dot(pt, dwg))

def mark_curves(t_data, side_up: str, tile_cmds, dwg):
    """Mark calculated points of each curved path."""
    red_dot_grp = dwg.g()
    #for dr_order in ["Early", "Late"]:
    for path_d in t_data[side_up]["ArcPaths"]:
        mark_arc_points(path_d, red_dot_grp, dwg)
    tile_cmds.add(red_dot_grp)

