# copyright 2023 Daniel P. Koppenheffer
"""mr_svg_mntns - Mountains and RidgeLines classes."""

import math

import svgwrite

from tile_gen.svg_const import \
    CLEARING_RADIUS, TILE_RADIUS, \
    HEX_EDGES, SKIP_INDICES, CLEARING_PTS

from tile_gen.geom_utils import \
    calc_arc_pt, angle_two_pts, \
    perpendicular_angle, compute_mid_pt, \
    segment_intersect, calc_polar_prctg_pt, define_chord, \
    segment_intersect_circle, round_pt

from tile_gen.misc_draw import \
    define_line, line_to, emplace_element, \
    place_defs_element, define_round_arc, draw_arc_path

MNTN_BACKGROUND = "#747474"
MNTN_FOREGROUND = "#2f2f2f"
RIDGE_END = "#3f3f3f"   # NOT IN USE
MNTN_SKIRT_UPPER = "#2b272b"
MNTN_SKIRT_LOWER = "#606060"
MNTN_VALE_GRAD_LOW = '#636363'
MNTN_VALE_GRAD_HIGH = '#313131'

# Mountain factors relative to Clearing Radius. Also used by RidgeLines.
MNTN_R_FACTOR = 2.3
EXTD_MNTN_R_FACTOR = 2.5
MNTN_CLR_MARGIN = 1.1

class RidgeLines:

    RIDGE_OPTIONS = 1   # to be set by ridge_elbows_deviations() method.
    # These factors apply to clearing radius. Indicates how far along
    # a ridge-line deviates, forks, etc.
    RIDGE_FORK_FACTORS = [1.3, 1.5, 1.7, 1.9]
    DEVIATION_FACTORS = [1.5, 1.7]
    ridge_opac = 0.8


    def __init__(self):
        # The offsets used to compute ridge line deviation end point(s).
        self.RIDGE_SPLIT_OFFSET = 8
        self.RIDGE_UPPER_WIDTH = 6
        self.RIDGE_LOWER_WIDTH = 6
        self.RIDGE_DEVIATION = 5
        self.ELBOW_RTN_FACTOR = 0.2

        # Ridge bottom relative to clearing radius.
        self.RIDGE_END_FACTOR = 2.15


    def mntn_skirt_gradient(self, lower_color, upper_color, mntn_radius, dwg):
        """Make inter-ridge ravines fade near mountain base."""
        grad = dwg.radialGradient(id='mntn_skirt_gradient', cx=0, cy=0,
                    r=mntn_radius, gradientUnits="userSpaceOnUse", opacity=1)
        grad.add_stop_color(offset="1%", color=upper_color)
        grad.add_stop_color(offset="60%", color=upper_color)
        grad.add_stop_color(offset="95%", color=lower_color)
        return grad

    def straight_ridge(self, ctr_pt, r_color, ridge_nm, dwg):
        """Draw ridge line straight to bottom of mountain skirt.
        """
        ridge_struct = dwg.g(id=ridge_nm)
        ridge_start_pt = calc_arc_pt(ctr_pt,
                round(CLEARING_RADIUS * MNTN_CLR_MARGIN, 3), 0)
        ridge_extn = calc_arc_pt(ctr_pt,
                round(CLEARING_RADIUS * self.RIDGE_END_FACTOR, 3), 0)
        ridge = dwg.line(tuple(ridge_start_pt), tuple(ridge_extn))
        ridge.stroke(r_color, width=self.RIDGE_UPPER_WIDTH,
                linecap='round', opacity=self.ridge_opac)
        ridge_struct.add(ridge)
        dwg.defs.add(ridge_struct)


    def ridge_elbow(self, ctr_pt, elb_start, elb_middle, elb_end,
                offset, bkgd_color, ridge_struct, dwg):
        """Elbow in ridge line.
        Ridge line deviates to one side and immediately back to
        original path, then continues to skirt bottom.
        """
        NUM = 1; DENOM = 16
        angle = math.pi * NUM / DENOM
        perp_angle = perpendicular_angle(angle)

        # diagonal to one side
        next_pt = calc_arc_pt(elb_middle, offset, perp_angle)
        ridge = dwg.line(tuple(elb_start), tuple(next_pt))
        ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                linecap='round', opacity=self.ridge_opac)
        ridge_struct.add(ridge)
        # diagonal back to original path
        ridge = dwg.line(tuple(next_pt), tuple(elb_end))
        ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                linecap='round', opacity=self.ridge_opac)
        ridge_struct.add(ridge)
        # straight down to bottom.
        ridge_extn = calc_arc_pt(ctr_pt,
                round(CLEARING_RADIUS * self.RIDGE_END_FACTOR, 3), 0)
        ridge = dwg.line(tuple(elb_end), tuple(ridge_extn))
        ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                        linecap='round', opacity=self.ridge_opac)
        ridge_struct.add(ridge)

    def ridge_deviate(self, ctr_pt, dev_start, dev_middle,
                offset, bkgd_color, ridge_struct, dwg):
        """Ridge line deviates to one side then continues to skirt
        bottom in parallel to original path.
        """
        NUM = 1; DENOM = 16
        angle = math.pi * NUM / DENOM
        perp_angle = perpendicular_angle(angle)

        # diagonal to one side
        next_pt = calc_arc_pt(dev_middle, offset, perp_angle)
        ridge = dwg.line(tuple(dev_start), tuple(next_pt))
        ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                linecap='round', opacity=self.ridge_opac)
        ridge_struct.add(ridge)
        # straight down to bottom, parallel to original path.
        ridge_extn = calc_arc_pt(ctr_pt,
                round(CLEARING_RADIUS * self.RIDGE_END_FACTOR, 3), 0)
        ridge_alt_extn = calc_arc_pt(ridge_extn, offset, perp_angle)
        ridge = dwg.line(tuple(next_pt), tuple(ridge_alt_extn))
        ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                        linecap='round', opacity=self.ridge_opac)
        ridge_struct.add(ridge)

    def ridge_elbows_deviations(self, bkgd_color, dwg):
        """Create elbow and deviation ridge-lines for dwg.defs.
        Serialized in no particular order. Serialization used to support
        random selection. Used in mountain skirts."""
        ctr_pt = [0,0]
        NUM = 1; DENOM = 16
        angle = math.pi * NUM / DENOM
        idx = 0
        for j in range(len(RidgeLines.DEVIATION_FACTORS)):
            ridge_deviatn_factor = RidgeLines.DEVIATION_FACTORS[j]
            # Draw upper ridge
            ridge_start_pt = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS * MNTN_CLR_MARGIN, 3), 0)
            deviatn_start = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS * ridge_deviatn_factor, 3), 0)
            deviatn_middle = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS *
                    (ridge_deviatn_factor + self.ELBOW_RTN_FACTOR), 3), 0)
            elbow_rtn = calc_arc_pt(ctr_pt, round(CLEARING_RADIUS *
                    (ridge_deviatn_factor +
                    (2 * self.ELBOW_RTN_FACTOR)), 3), 0)
            ridge = dwg.line(tuple(ridge_start_pt), tuple(deviatn_start))
            ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                    linecap='round', opacity=self.ridge_opac)
            for k in range(2):
                deviatn_nm = f'deviatn_{str(idx)}'
                idx += 1
                deviatn_struct = dwg.g(id=deviatn_nm)
                deviatn_struct.add(ridge)

                ridge_deviation = self.RIDGE_DEVIATION
                if k == 1:
                    ridge_deviation = -self.RIDGE_DEVIATION
                self.ridge_elbow(ctr_pt, deviatn_start, deviatn_middle,
                        elbow_rtn, ridge_deviation, bkgd_color,
                        deviatn_struct, dwg)

                deviatn_struct.add(ridge)
                dwg.defs.add(deviatn_struct)

            for k in range(2):
                deviatn_nm = f'deviatn_{str(idx)}'
                idx += 1
                deviatn_struct = dwg.g(id=deviatn_nm)
                deviatn_struct.add(ridge)

                ridge_deviation = self.RIDGE_DEVIATION
                if k == 1:
                    ridge_deviation = -self.RIDGE_DEVIATION
                self.ridge_deviate(ctr_pt, deviatn_start, deviatn_middle,
                        ridge_deviation, bkgd_color, deviatn_struct, dwg)

                deviatn_struct.add(ridge)
                dwg.defs.add(deviatn_struct)

        deviatn_nm = f'deviatn_{str(idx)}'
        self.straight_ridge(ctr_pt, bkgd_color, deviatn_nm, dwg)
        idx += 1
        RidgeLines.RIDGE_OPTIONS = idx

    def fork_ridge(self, horz_offset, div_pt, end_pt, angle, r_color, dwg):
        """Create one half of a ridge-line fork.
        """
        ridge_end = calc_arc_pt(end_pt, horz_offset, angle)
        ridge = dwg.line(tuple(div_pt), tuple(ridge_end))
        ridge.stroke(r_color, width=self.RIDGE_LOWER_WIDTH,
                linecap='round', opacity=self.ridge_opac)
        return ridge

    def ridge_fork_factory(self, bkgd_color, dwg):
        """Generate and cache forked ridges in dwg.defs.
        """
        ctr_pt = [0,0]
        NUM = 1; DENOM = 16
        angle = math.pi * NUM / DENOM

        # Four variations relative to where fork begins.
        for j in range(len(RidgeLines.RIDGE_FORK_FACTORS)):
            ridge_nm = f'ridge_fork_{str(RidgeLines.RIDGE_FORK_FACTORS[j])}'
            ridge_struct = dwg.g(id=ridge_nm)
            ridge_fork_factor = RidgeLines.RIDGE_FORK_FACTORS[j]
            # Draw straight upper ridge line
            ridge_start_pt = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS * MNTN_CLR_MARGIN, 3), 0)
            ridge_mid_pt = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS * ridge_fork_factor, 3), 0)
            ridge = dwg.line(tuple(ridge_start_pt), tuple(ridge_mid_pt))
            ridge.stroke(bkgd_color, width=self.RIDGE_UPPER_WIDTH,
                            linecap='round', opacity=self.ridge_opac)
            ridge_struct.add(ridge)
            # Draw forking ridge to mountain base.
            fork_ext = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS * self.RIDGE_END_FACTOR, 3), 0)
            perp_angle = perpendicular_angle(angle)
            div_pt = calc_arc_pt(ctr_pt,
                    round(CLEARING_RADIUS * ridge_fork_factor, 3), 0)
            for sign in (-1, 1):
                ridge_struct.add(self.fork_ridge(
                        sign * self.RIDGE_SPLIT_OFFSET,
                        div_pt, fork_ext, perp_angle, bkgd_color, dwg))
            dwg.defs.add(ridge_struct)

    def ridge_factory(self, dwg):
        """Create ridge line elements in dwg.defs."""
        self.ridge_fork_factory(MNTN_BACKGROUND, dwg)
        self.ridge_elbows_deviations(MNTN_BACKGROUND, dwg)
        mntn_radius = round(CLEARING_RADIUS * MNTN_R_FACTOR, 3)
        dwg.defs.add(self.mntn_skirt_gradient(
                        MNTN_SKIRT_LOWER, MNTN_SKIRT_UPPER,
                        mntn_radius, dwg))


def discard_swp_angles(segs_w_angles: list):
    """Strip out per-point sweep angle; return segment list."""
    segs_out = []
    for elem in segs_w_angles:
        segs_out.append(elem["segment"])
    return segs_out


def sort_chords(skirt_segs: list, ctr_pt):
    """Attach sweep angle to each point; sort segments clockwise.

    Each segment is a circle chord.  A segment whose second point is
    "earlier" than first means it crosses the zero angle threshold.
    """
    segs_w_angles = []
    for seg in skirt_segs:
        # Sweep angles of intersection of chord point pairs.
        rot_pair = []
        rot_pair.append(round(angle_two_pts(ctr_pt, seg[0]), 3))
        rot_pair.append(round(angle_two_pts(ctr_pt, seg[1]), 3))
        segs_w_angles.append(
                {"angle": rot_pair, "segment": seg})

    # Re-sort rot-pairs into clockwise order,
    if len(segs_w_angles) > 1:
        segs_w_angles.sort(key=lambda elem: elem["angle"][0])
    return segs_w_angles


def clip_circle(ctr_pt, r, skirt_segs, dwg):
    """Clip arcs defined by segments from a circle, return path.

    Used to create a clipping path around circular mountains when they
    impinge on another mountain or hex edge.
    This is a look-ahead algorithm; look-ahead may trigger exception case.
    Normal case is to draw the current chord then an arc stopping at
    the next chord's starting pt. Exception is if current chord intersects
    the next chord, then draw from current chord start point to
    intersection pt; no arc drawn for exception case.
    After processing all chords except last, if loop ended when NOT
    processing the exception case, then draw final chord plus arc back
    to starting point of initial chord."""
    # Sort chords in order of initial point sweep angle, i.e., clockwise.
    segs_w_angles = sort_chords(skirt_segs, ctr_pt)
    sweep_flag = 1
    no_intervening_arc = False
    # If initial segment intersects with last segment AND
    # the last segment crosses zero degrees (angle[0] > angle[1]
    # then start from intersection point. 
    # Plus, final segment or arc needs to finish at that intersection point.
    #
    # Before beginning, see if initial and last segment intersect.
    idx_last = len(segs_w_angles) -1
    chord_intersect_pt = None
    if len(segs_w_angles) > 1 and \
            segs_w_angles[idx_last]["angle"][0] > \
            segs_w_angles[idx_last]["angle"][1]:
        chord_intersect_pt = segment_intersect(
                segs_w_angles[0]["segment"],
                segs_w_angles[idx_last]["segment"])
    if chord_intersect_pt:
        # Start from intersection point.
        clip_cmd = dwg.path(d=("M", 
                chord_intersect_pt[0],
                chord_intersect_pt[1]))
        start_pt = chord_intersect_pt
    else:
        # Move to start pt of initial chord.
        clip_cmd = dwg.path(d=("M", 
                segs_w_angles[0]["segment"][0][0],
                segs_w_angles[0]["segment"][0][1]))
        start_pt = segs_w_angles[0]["segment"][0]
    for idx in range(len(segs_w_angles)):
        chord_intersect_pt = None
        # Check if current chord intersects with next chord,
        # or if last chord, if it intersects with initial chord.
        if len(segs_w_angles) > 1:
            if idx == idx_last:
                # Don't wrap around if two segments: 
                # gives same intersection twice.
                #chord_intersect_pt = start_pt
                if idx != 1:
                    chord_intersect_pt = segment_intersect(
                            segs_w_angles[idx]["segment"],
                            segs_w_angles[0]["segment"])
            else:
                # Chk versus next chord
                chord_intersect_pt = segment_intersect(
                        segs_w_angles[idx]["segment"],
                        segs_w_angles[idx + 1]["segment"])
        if chord_intersect_pt:
            # If intersect next chord, draw line to intersection instead
            # of to current chord's end pt.
            clip_cmd.push(define_line(chord_intersect_pt))
            no_intervening_arc = True
        else:
            # Ordinary case: draw chord followed by arc to start of
            # next chord.
            no_intervening_arc = False
            clip_cmd.push(define_line(segs_w_angles[idx]["segment"][1]))
            if idx == idx_last:
                # If no more chords, arc to initial seg starting pt.
                clip_cmd.push(define_round_arc(
                            segs_w_angles[0]["segment"][0],
                            r, segs_w_angles[idx]["angle"][1],
                            segs_w_angles[0]["angle"][0],
                            sweep_flag))
            else:
                # Normal case: arc to next chord starting pt.
                clip_cmd.push(define_round_arc(
                            segs_w_angles[idx + 1]["segment"][0],
                            r, segs_w_angles[idx]["angle"][1],
                            segs_w_angles[idx + 1]["angle"][0],
                            sweep_flag))
    if no_intervening_arc:
        if start_pt != segs_w_angles[0]["segment"][0]:
            clip_cmd.push(define_line(start_pt))
        else:
            clip_cmd.push(define_line(segs_w_angles[idx]["segment"][1]))
            # If stopped during 'no intervening arc',
            # must draw arc to start pt.
            clip_cmd.push(define_round_arc(
                    segs_w_angles[0]["segment"][0],
                    r, segs_w_angles[-1]["angle"][1],
                    segs_w_angles[0]["angle"][0],
                    sweep_flag))

    return clip_cmd


class Mountains:
    """Draw mountains."""

    def __init__(self, rand_ng, rocks_obj):
        self.rng = rand_ng
        self.rocks = rocks_obj
        # For visually validating clipping paths.
        self.SKIP_INDICES = SKIP_INDICES

    def define_mntn_intersect_grads(self, base_nm, intersect_tbl, dwg):
        """Define unique gradient for each inter-mountain high valley.
        """
        skirt_grad_urls = []
        for idx in range(len(intersect_tbl)):
        #for idx, seg in enumerate(intersect_tbl):
            # Then define the matching gradient...
            grad_url = f'skirt_gradient_{base_nm}_{idx}'
            skirt_grad_urls.append(grad_url)
            skirt_grad = dwg.linearGradient(id=grad_url)
            skirt_grad.add_stop_color(offset="1%",
                    color=MNTN_VALE_GRAD_LOW)
            skirt_grad.add_stop_color(offset="50%",
                    color=MNTN_VALE_GRAD_HIGH)
            skirt_grad.add_stop_color(offset="100%",
                    color=MNTN_VALE_GRAD_LOW)
            dwg.defs.add(skirt_grad)
        return skirt_grad_urls

    def place_forking_ridge(self, ctr_pt, angle, dwg):
        """Places a random forked ridge."""
        fork_idx = self.rng.integers(len(RidgeLines.RIDGE_FORK_FACTORS))
        ridge_nm = \
                f'ridge_fork_{str(RidgeLines.RIDGE_FORK_FACTORS[fork_idx])}'
        angle_deg = round(math.degrees(angle), 3)
        place_fork = place_defs_element(ridge_nm, ctr_pt, angle_deg, dwg)
        return place_fork

    def place_deviatn_ridge(self, ctr_pt, angle, dwg):
        """Places a random deviation ridge."""
        deviatn_idx = self.rng.integers(RidgeLines.RIDGE_OPTIONS)
        deviatn_nm = f'deviatn_{str(deviatn_idx)}'
        angle_deg = round(math.degrees(angle), 3)
        place_ridge = place_defs_element(deviatn_nm, ctr_pt, angle_deg, dwg)
        return place_ridge

    def draw_mntn_intersects(self, mntn_intersects, grad_urls, dwg):
        """Draws the 'valley' where two mntn skirts intersect."""
        intersect = dwg.g()
        for idx, seg in enumerate(mntn_intersects):
            # Need to use the segments as modified for clipping, the
            # 'raw' segments draw over other elements. Else, clip these.
            # See define_mntn_intersect_grads()
            stroke_url = f'url(#{grad_urls[idx]})'
            intersect_ln = define_chord(seg, dwg)
            intersect_ln.stroke(stroke_url, linecap='butt', width=5)
            intersect.add(intersect_ln)
        return intersect

    def draw_radial_ridge_lines(self, ctr_pt, clr_r, mtn_skirt, dwg):
        """Add ridge lines extending radially from center, to mtn_skirt."""
        DENOM = 24
        for num in range (0, 48):
            angle = math.pi * num / DENOM
            # Ridge lines on odd numerators..
            if num % 2 == 1:
                # Either ridge fork or ridge deviation
                rand_choice = self.rng.integers(7)
                if rand_choice in [0, 1, 2]:
                    mtn_skirt.add(
                            self.place_forking_ridge(ctr_pt, angle, dwg))
                else:
                    mtn_skirt.add(
                            self.place_deviatn_ridge(ctr_pt, angle, dwg))

            # Fallen rocks, boulders surround mountain base.
            rock_loc = calc_arc_pt(ctr_pt, clr_r * MNTN_R_FACTOR, angle)
            mtn_skirt.add(self.rocks.fallen_rocks(rock_loc, dwg))

            # Even numerator for ravine btwn ridge lines...
            if num % 2 == 0:
                # May draw string of pebbles near ravine bottom.
                if self.rng.integers(5) in [0,1]:
                    peb_string = self.rocks.string_of_pebbles(dwg)
                    emplace_element(peb_string, rock_loc,
                        (round(math.degrees(angle) + 180, 3)), mtn_skirt)

    def draw_mountain_skirt_w_intersect(self, t_name, ctr_pt, clr_r, \
            bkgd_color, skirt_segs, hi_valley_segs, dwg):
        """Draw descending ridgelines around a mountain top.
        'skirt' refers to circular mountain's ridged slope."""

        # Provide extended radius to display rock debris around mountain base.
        mntn_ext_radius = round(clr_r * EXTD_MNTN_R_FACTOR, 3)

        # If mntn base intersects something, define clipping path.
        if len(skirt_segs):
            # Use mntn_ext_radius to compute radius including rock-fall.
            xx_yy = f'{t_name}_{math.trunc(ctr_pt[0])}' \
                    + f'_{math.trunc(ctr_pt[1])}'
            clip_path = dwg.clipPath(id=f'skirt_clip_{xx_yy}')
            clipping_path = clip_circle(ctr_pt, mntn_ext_radius,
                    skirt_segs, dwg)
            clip_path.add(clipping_path)
            mtn_clip_url = clip_path.get_funciri()
            dwg.defs.add(clip_path)

        # Mountain intersection gradients: high valley's.
        intersect_grad_urls = None
        if len(hi_valley_segs):
            intersect_grad_urls = self.define_mntn_intersect_grads(
                        xx_yy, hi_valley_segs, dwg)

        # Apply mntn intersection gradients above mountain clipping
        # (mtn_root group) else they'll not appear.
        mtn_root = dwg.g()
        mtn_skirt = dwg.g()
        mtn_root.add(mtn_skirt)

        if len(skirt_segs):
            mtn_skirt.__setitem__("clip-path", mtn_clip_url)

        # Ravine background
        mntn_radius = round(clr_r * MNTN_R_FACTOR, 3)
        skirt = dwg.circle(tuple([0,0]), mntn_radius)
        skirt.fill('url(#mntn_skirt_gradient)')
        skirt.translate(tuple(ctr_pt))
        mtn_skirt.add(skirt)

        # Mountain top surrounding clearing
        skirt = dwg.circle(tuple(ctr_pt), round(clr_r * MNTN_CLR_MARGIN, 3))
        skirt.fill(bkgd_color, opacity=1)
        mtn_skirt.add(skirt)

        self.draw_radial_ridge_lines(ctr_pt, clr_r, mtn_skirt, dwg)

        if len(hi_valley_segs):
            mtn_root.add(self.draw_mntn_intersects(hi_valley_segs,
                            intersect_grad_urls, dwg))
        ### Debug clipping path
        if self.SKIP_INDICES:
            # Draw clipping path in red!
            draw_clip_path = clipping_path
            draw_clip_path.stroke('red', width=3)
            draw_clip_path.fill(None, opacity=0)
            mtn_root.add(draw_clip_path)
        ###
        return mtn_root

    def interpeak_boundary(self, this_peak, that_peak, skirt_r):
        """Find the line segment where two circles of the same
        radius overlap.  """
        d = math.dist(this_peak, that_peak)
        side_b = d / 2
        side_c = skirt_r
        side_a = math.sqrt((side_c * side_c) - (side_b * side_b))
        intersect_angle = angle_two_pts(this_peak, that_peak)
        perpen_angle = perpendicular_angle(intersect_angle)
        mid_peak = compute_mid_pt(this_peak, that_peak)
        intersect_1 = calc_arc_pt(mid_peak, -side_a, perpen_angle)
        intersect_2 = calc_arc_pt(mid_peak, side_a, perpen_angle)
        return [intersect_1, intersect_2]

    def calc_intersect_segments(self, mntns, r_factor,
            intersect_segms, mntn_ctr_pt, mntn_idx):
        """Calculate if any tile edges or any mountains intersect.

        mntns - list of mountains already drawn
        r_factor - clearing radius multiplier to obtain proper radius
        intersect_segms - list of chords where current mntn
            intersects with either edges or another mountain.
        mntn_ctr_pt - center pt of current mountain
        mntn_idx - index of 'this' mntn; will work 'those' mntns,
            already drawn which are less than the index.
        """
        intersect_segms.clear()
        # Clip mountains at hex edges.
        for edge in HEX_EDGES:
            chord = segment_intersect_circle(mntn_ctr_pt,
                    r_factor * CLEARING_RADIUS, edge)
            if chord:
                intersect_segms.append(chord)
        # Clip versus previously drawn mountains.
        for j in range(mntn_idx):
            that_clr_ctr_pt = CLEARING_PTS[int(mntns[j][0])]
            # If mntn overlaps neighboring mntn...
            if math.dist(mntn_ctr_pt, that_clr_ctr_pt) < \
                        2 * r_factor * CLEARING_RADIUS:
                # Add to seg list.
                intersect_chord = self.interpeak_boundary(
                        mntn_ctr_pt, that_clr_ctr_pt,
                        r_factor * CLEARING_RADIUS)
                intersect_segms.append(intersect_chord)


    def draw_mountains(self, t_data, side_up: str, tile_cmds, dwg):
        """Draw circular mountains; clip where sides overlap.

        t_data - raw tile data file data; use 'TileGeography' for
            clearings and 'TileName' to uniquely ID specific mntn-tile
            gradients.
        side_up - "Normal" or "Enchanted" (front or back)
        tile_cmds - Accruing group of SVG commands.
        """
        global CLEARING_PTS, HEX_EDGES
        mntns = []
        g_data = t_data["TileGeography"]
        # Process only mntn clearings...
        for clr_num, clr in g_data[side_up]["Clearings"].items():
            if clr["ClrType"] == "mountain":
                mntns.append([clr_num, clr])

        clip_intersect_segs = []
        mntn_intersect_segs = []
        # Accrue two chord intersection sets:
        #       clip_intersect_segs for clipping around mountain base;
        #       inter_mntn_chords to draw 'high valley' grad btwn mntns.
        for idx, this_clr_d in enumerate(mntns):
            this_clr_ctr_pt = CLEARING_PTS[int(this_clr_d[0])]
            self.calc_intersect_segments(mntns, EXTD_MNTN_R_FACTOR,
                    clip_intersect_segs, this_clr_ctr_pt, idx)

            self.calc_intersect_segments(mntns, MNTN_R_FACTOR,
                    mntn_intersect_segs, this_clr_ctr_pt, idx)
            inter_mntn_chords = \
                    self.clip_intersecting_segmts(mntn_intersect_segs,
                            this_clr_ctr_pt)

            # Debug aid: skip mntn so clipping path may display.
            if idx in self.SKIP_INDICES:
                continue

            tile_cmds.add(self.draw_mountain_skirt_w_intersect(
                        t_data["TileName"].replace(" ", "_"),
                        this_clr_ctr_pt, CLEARING_RADIUS,
                        MNTN_BACKGROUND, clip_intersect_segs,
                        inter_mntn_chords, dwg))


    def clip_intersecting_segmts(self, mntn_intersect_segs, ctr_pt):
        """Clip off segments beyond intersections."""
        sorted_w_angles = sort_chords(mntn_intersect_segs, ctr_pt)
        segs_out = discard_swp_angles(sorted_w_angles)
        for idx in range(len(segs_out)):
            chord_intersect_pt = None
            # Check if current chord intersects with next chord,
            # or if last chord, if it intersects with initial chord.
            if len(segs_out) > 1:
                if idx == len(segs_out) -1:
                    # Chk versus initial chord
                    if idx != 1:
                        # Skip if only two chords because wrapping
                        # around to first chord is duplicative processing.
                        chord_intersect_pt = segment_intersect(
                                segs_out[idx],
                                segs_out[0])
                        if chord_intersect_pt:
                            # Clip end of current segment and
                            # start of init pt.
                            segs_out[idx][1] = chord_intersect_pt
                            segs_out[0][0] = chord_intersect_pt
                else:
                    # Chk versus next chord
                    chord_intersect_pt = segment_intersect(
                            segs_out[idx],
                            segs_out[idx + 1])
                    if chord_intersect_pt:
                        # Clip end of current segment and start of next pt.
                        segs_out[idx][1] = chord_intersect_pt
                        segs_out[idx + 1][0] = chord_intersect_pt
        return segs_out


