# copyright 2023 Daniel P. Koppenheffer
"""Math and geometry utilities primarily supporting SVG creation.
"""

import math

from tile_gen.svg_const import TILE_RADIUS


def calc_arc_pt(pt, r, rads):
    """Define a point at r dist and angle from given point. """
    pt = round_pt([
        r * math.cos(rads) + pt[0],
        r * math.sin(rads) + pt[1]
    ])
    for i in [0, 1]:
        if pt[i] == -0.0:
            pt[i] = 0.0
    return pt

def angle_two_pts(start_pt, end_pt):
    """Return the angle btwn two points."""
    y_diff = (end_pt[1] - start_pt[1])
    x_diff = (end_pt[0] - start_pt[0])
    return clockwise_angle(math.atan2(y_diff, x_diff))

def slope(pt1, pt2):
    """Return the slope btwn two points."""
    y_diff = (pt2[1] - pt1[1])
    x_diff = (pt2[0] - pt1[0])
    return y_diff/x_diff

def perpendicular_angle(angle):
    """Angle in radians."""
    return angle + (math.pi / 2)

def clockwise_angle(angle):
    """Chg negative radian angle to its positive equivalent."""
    if angle < 0:
        return angle + (2 * math.pi)
    return angle

def compute_mid_pt(pt1: list, pt2: list):
    """Return the mid-point btwn two points."""
    cp = [0,0]
    if pt1[0] > pt2[0]:
        cp[0] = pt1[0] - ((pt1[0] - pt2[0]) / 2)
    else:
        cp[0] = pt2[0] - ((pt2[0] - pt1[0]) / 2)
    if pt1[1] > pt2[1]:
        cp[1] = pt1[1] - ((pt1[1] - pt2[1]) / 2)
    else:
        cp[1] = pt2[1] - ((pt2[1] - pt1[1]) / 2)
    return cp

# A segment is a list of two pt lists:
#   [ [x1, y1], [x2, y2] ]

def segment_vertical(segment):
    """Returns True if a line segment is vertical else False"""
    if segment[0][0] == segment[1][0]:
        return True
    return False

def x_on_segment(seg, pt):
    """Does point lie on segment?
    Prerequisite is that line intersection has been verified."""
    if min(seg[0][0], seg[1][0]) <= pt[0] <= max(seg[0][0], seg[1][0]):
        return True

def abbrev_seg_ends(seg, less_amt, abbrev_ends="both"):
    """Clip less_amt from either end of a line segment.

    Abbreviates both ends of the segment by default. 
    Parameter abbrev_ends determines which ends of segment are abbreviated. 
    abbrev_ends values: "both" abbreviate both ends; default,
    "start": abbreviate only the starting end,
    "end": abbreviate only the 'ending' end.
    Any other string turns off abbreviation for both ends.
    """
    angle = angle_two_pts(seg[0], seg[1])
    seg_rtn = [0,0]
    seg_rtn[0] = calc_arc_pt(seg[0], less_amt, angle)
    seg_rtn[1] = calc_arc_pt(seg[1], -less_amt, angle)
    if abbrev_ends == "both":
        pass
    elif abbrev_ends == "start":
        seg_rtn[1] = seg[1]
    elif abbrev_ends == "end":
        seg_rtn[0] = seg[0]
    else:
        seg_rtn[0] = seg[0]
        seg_rtn[1] = seg[1]
    return seg_rtn

def abbrev_arc_ends(start_pt, end_pt, ctr_pt, less_amt):
    """Clip less_amt from length of arc at both ends."""
    start_rad = angle_two_pts(ctr_pt, start_pt)
    end_rad = angle_two_pts(ctr_pt, end_pt)
    radius = math.dist(ctr_pt, start_pt)
    swp_total = calc_arc_sweep(start_rad, end_rad)
    total_dist = calc_sweep_dist(radius, start_rad, end_rad)
    cvrd_prctg = round(less_amt / total_dist, 3)
    angle = start_rad + (swp_total * cvrd_prctg)
    new_start = calc_arc_pt(ctr_pt, less_amt, angle)
    angle = end_rad - (swp_total * cvrd_prctg)
    new_end = calc_arc_pt(ctr_pt, less_amt, angle)
    return [new_start, new_end]


def round_pt(seg):
    """Round point values to 3 decimal places."""
    seg[0] = round(seg[0], 3)
    seg[1] = round(seg[1], 3)
    return seg

def arc_lens_in_half_circle(radius, share_width):
    """Calculate 'Shares' fit in half-circle for radius and share_width.
    """
    half_circum = circumference(radius) / 2
    return math.ceil(half_circum / share_width)

def segment_intersect(seg1, seg2):
    """Return intersection point [x,y] of two segments or None."""
    intersect_pt = compute_ln_intersect(seg1, seg2)

    if not intersect_pt:
        return None
    # Is intersection within segments.
    if x_on_segment(seg1, intersect_pt) and x_on_segment(seg2, intersect_pt):
        return round_pt(intersect_pt)
    return None

def intersect_within_radius(seg1, seg2, ctr_pt, r):
    """ If segments intersect within circle, return pt, else return None.
    """
    intersect_pt = segment_intersect(seg1, seg2)
    if intersect_pt:
        if math.dist(intersect_pt, ctr_pt) < r:
            return intersect_pt
    return None

# https://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm
def segment_intersect_circle(ctr_pt, circle_r, seg):
    """Compute chord where segment intersects circle."""
    seg_len = math.dist(seg[0], seg[1])

    seg_vec_x = (seg[1][0] - seg[0][0]) / seg_len
    seg_vec_y = (seg[1][1] - seg[0][1]) / seg_len

    # Find nearest seg pt to circle center.
    t =       seg_vec_x * (ctr_pt[0] - seg[0][0]) \
            + seg_vec_y * (ctr_pt[1] - seg[0][1])
    nearest_seg_pt = [
        t * seg_vec_x + seg[0][0],
        t * seg_vec_y + seg[0][1]
    ]

    # Shortest dist from circle center to segment.
    seg_dist = math.dist(nearest_seg_pt, ctr_pt)

    if seg_dist < circle_r:
        dt = math.sqrt(pow(circle_r, 2) - pow(seg_dist, 2))
        chord = [
            round_pt([
                (t - dt) * seg_vec_x + seg[0][0],
                (t - dt) * seg_vec_y + seg[0][1]
            ]),
            round_pt([
                (t + dt) * seg_vec_x + seg[0][0],
                (t + dt) * seg_vec_y + seg[0][1]
            ])
        ]
        return chord
    return None


def chord_at_dist(circle_r, chord_dist, chord_ctr, angle):
    """Return chord at chord_dist from circle center.
    Assumes already verified chord intersects circle."""
    side_b = round(chord_dist / 2, 3)
    side_c = circle_r
    side_a = round(math.sqrt((side_c * side_c) - (side_b * side_b)), 3)
    intersect_1 = calc_arc_pt(chord_ctr, -side_a, angle)
    intersect_2 = calc_arc_pt(chord_ctr, side_a, angle)
    return [intersect_1, intersect_2]


def frac_to_radians(num, denom):
    """Given numerator and denominator, returns implied radian value."""
    return math.pi * int(num) / int(denom)

def calc_polar_prctg_pt(pos_d, relative_pt):
    """Return cartesian pt relative to another cartesian pt translated
    by a polar tile angle/radius set. """
    rads = frac_to_radians(pos_d["Numerator"], pos_d["Denominator"])
    pt = calc_arc_pt(relative_pt, \
            float(pos_d["RadiusPrctg"]) * TILE_RADIUS, rads)
    return pt

def calc_arc_sweep(start_rads, end_rads):
    """Return diff btwn start and end of radian angles even
    when start angle is negative (as returned by atan2)."""
    begin = clockwise_angle(start_rads)
    end = clockwise_angle(end_rads)
    if begin > end:
        return (math.pi *2) - begin + end
    else:
        return end - begin

    """
    if start_rads > end_rads:
        return end_rads + (2 * math.pi) - start_rads
    else:
        return end_rads - start_rads
    """
def circumference(radius):
    """Calculate circle circumference."""
    return math.pi * 2 * radius

def calc_sweep_dist(radius, start_rads, end_rads):
    circumf = circumference(radius)
    sweep = calc_arc_sweep(start_rads, end_rads)
    prtg = sweep / (math.pi * 2)
    dist = circumf * prtg
    return dist

def find_circle(pt1, pt2, pt3):
    """Given three circle circumference points, return center and radius."""
    # https://www.geeksforgeeks.org/equation-of-circle-when-three-points-on-the-circle-are-given/
    x12 = (pt1[0] - pt2[0])
    x13 = (pt1[0] - pt3[0])

    y12 = (pt1[1] - pt2[1])
    y13 = (pt1[1] - pt3[1])

    y31 = (pt3[1] - pt1[1])
    y21 = (pt2[1] - pt1[1])

    x31 = (pt3[0] - pt1[0])
    x21 = (pt2[0] - pt1[0])

    #pt1[0]^2 - pt3[0]^2
    sx13 = math.pow(pt1[0], 2) - math.pow(pt3[0], 2)

    # pt1[1]^2 - pt3[1]^2
    sy13 = math.pow(pt1[1], 2) - math.pow(pt3[1], 2)

    sx21 = math.pow(pt2[0], 2) - math.pow(pt1[0], 2)
    sy21 = math.pow(pt2[1], 2) - math.pow(pt1[1], 2)

    f = ((sx13) * (x12) \
            + (sy13) * (x12) \
            + (sx21) * (x13) \
            + (sy21) * (x13)) \
            / (2 * ((y31) * (x12) - (y21) * (x13)))
    g = ((sx13) * (y12) \
            + (sy13) * (y12) \
            + (sx21) * (y13) \
            + (sy21) * (y13)) \
            / (2 * ((x31) * (y12) - (x21) * (y13)))

    c = -(math.pow(pt1[0], 2)) - \
            math.pow(pt1[1], 2) - 2 * g * pt1[0] - 2 * f * pt1[1]

    # eqn of circle be
    # x^2 + y^2 + 2*g*x + 2*f*y + c = 0
    # where centre is (h = -g, k = -f) and radius r
    # as r^2 = h^2 + k^2 - c
    h = -g
    k = -f
    sqr_of_r = h * h + k * k - c

    # r is the radius
    r = math.sqrt(sqr_of_r)

    return [h, k, r]

def define_chord(seg, dwg):
    chord_cmd = dwg.line(tuple(seg[0]), tuple(seg[1]))
    return chord_cmd

def display_bug_workaround(angle):
    """Avoids a gnome graphic lib display bug (or my divide by zero bug).

    Bug: suppresses linear gradient when rotated to vertical. 
    Adjust angle slightly off from vertical."""
    if math.degrees(angle) in [90.0, -90.0, 270.0, -270.0]:
        return angle + 0.001
    return angle

def compute_intersect_w_vertical(seg, x1):
    """When other segment (with x1) is vertical."""
    a34 = (seg[1][1] - seg[0][1]) / (seg[1][0] - seg[0][0])
    b34 = seg[0][1] - a34 * seg[0][0]
    y = a34 * x1 + b34
    return [x1, y]

def compute_ln_intersect(seg1, seg2):
    """
    Find intersection point of two lines.

    Return None if no intersecting point.
    Modeled after:
    https://stackoverflow.com/questions/16314069/
        calculation-of-intersections-between-line-segments
        (user name sashkello)
    and
    https://github.com/baobabKoodaa/CodeForcesLibrary/blob/
        ad6972b1ccd87c9100171122692562704c695703/src/baobab/
        A.java (segmentsIntersect)
    """

    # If both vertical including overlapping, return None.
    if segment_vertical(seg1) and segment_vertical(seg2):
        return None

    intrsct_pt = [0, 0]

    # If either line is vertical
    if seg1[0][0] == seg1[1][0]:
        intrsct_pt = compute_intersect_w_vertical(seg2, seg1[0][0])
    elif seg2[0][0] == seg2[1][0]:
        intrsct_pt = compute_intersect_w_vertical(seg1, seg2[0][0])
    # Neither line is vertical
    else:
        a12 = (seg1[1][1]-seg1[0][1])/(seg1[1][0]-seg1[0][0])
        b12 = seg1[0][1] - a12 * seg1[0][0]
        a34 = (seg2[1][1]-seg2[0][1])/(seg2[1][0]-seg2[0][0])
        b34 = seg2[0][1] - a34 * seg2[0][0]
        # Avoid division by (near) zero
        if abs(a12 - a34) < 0.001:
            return None

        intrsct_pt[0] = -(b12-b34)/(a12-a34)
        intrsct_pt[1] = a12 * intrsct_pt[0] + b12

    return round_pt(intrsct_pt)

def dual_circle_intersects(ctr_1, r_1, ctr_2, r_2):
    """Intersection points of two circles.

https://stackoverflow.com/questions/3349125/circle-circle-intersection-points
    """
    ctr_dist = math.dist(ctr_1, ctr_2)
    if ctr_dist > r_1 + r_2:
        return None
    if ctr_dist < abs(r_1 - r_2):
        return None
    if ctr_dist == 0 and r_1 == r_2:
        return None

    a = (math.pow(r_1, 2) - math.pow(r_2, 2) + math.pow(ctr_dist, 2)) / \
            (2 * ctr_dist)

    h = math.sqrt(math.pow(r_1, 2) - math.pow(a, 2))

    #P2 = P0 + a ( P1 - P0 ) / d
    p2_x = ctr_1[0] + a * (ctr_2[0] - ctr_1[0]) / ctr_dist
    p2_y = ctr_1[1] + a * (ctr_2[1] - ctr_1[1]) / ctr_dist
    p2 = [p2_x, p2_y]

    #x3 = x2 +- h ( y1 - y0 ) / d
    #y3 = y2 -+ h ( x1 - x0 ) / d

    p3a_x = p2[0] + h * (ctr_2[1] - ctr_1[1]) / ctr_dist
    p3a_y = p2[1] - h * (ctr_2[0] - ctr_1[0]) / ctr_dist
    p3a = [p3a_x, p3a_y]

    p3b_x = p2[0] - h * (ctr_2[1] - ctr_1[1]) / ctr_dist
    p3b_y = p2[1] + h * (ctr_2[0] - ctr_1[0]) / ctr_dist
    p3b = [p3b_x, p3b_y]

    return[p3a, p3b]
