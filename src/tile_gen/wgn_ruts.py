# copyright 2023 Daniel P. Koppenheffer
"""mr_wgn_ruts.py - draw stripes of varying length in parallel.
The intention is to create an impression of wagon ruts.
"""

import math

from tile_gen.geom_utils import \
    calc_arc_pt, angle_two_pts, \
    perpendicular_angle, calc_arc_sweep, calc_sweep_dist

from tile_gen.misc_draw import \
    draw_arc_path, create_cartes_path


class WagonRuts:
    """Draws parallel stripes resembling wagon ruts.

    Ruts (stripes) are of random length, start and end points within a
    lane (track). Methods for straight lines and circular arcs.
    """
    def __init__(self, path_width, rut_color, rand_ng):
        self.p_width = path_width
        self.rut_color = rut_color
        self.rng = rand_ng
        self.RUT_WIDTH = 3
        # With 36 wide path, 36 / 9 yields four rut 'tracks'
        self.TRK_WIDTH = 9
        self.LOW_BND_LEN = 7
        self.UP_BND_LEN = 31

        # Used by draw_clearing_ruts() method.
        # Values are rotation Start and End Degrees, % of Radius.
        self.CLRING_RUT_COORDS = [
            [128, 210, 0.24],
            [275, 360, 0.36],
            [1, 60, 0.36],
            [260, 315, 0.52],
            [5, 25, 0.52],
            [65, 125, 0.52],
            [15, 75, 0.79],
            [100, 180, 0.67],
            [205, 215, 0.67],
            [170, 275, 0.84],
            [240, 360, 0.67],
            [160, 220, 0.47],
            [290, 350, 0.16],
            [90, 150, 0.91],
            [305, 340, 0.91],
        ]

    def draw_arc_path_ruts(self, ctr_pt, r, start_rads, end_rads,
                            stripe_ct, stripe_grp, dwg):
        """Randomly draw stripes (ruts) on an arc path.

        Path width determines number of tracks to accomodate.
        """

        track_count = math.floor(self.p_width / self.TRK_WIDTH)
        track_arcs_radii = []
        lower_track_count = 0
        track_offset = 0
        mid_track = 0

        # Calculate radii of each track's arc (track_arcs_radii).
        if track_count % 2 == 1:
            # Odd track count:
            # Subtract one from count, then evenly divide remaining
            # tracks between each side of centerline track
            lower_track_count = int((track_count -1) / 2)
            mid_track = lower_track_count + 1
            for x in range(lower_track_count):
                track_offset = (lower_track_count - x) * self.TRK_WIDTH
                track_arcs_radii.append(r - track_offset)
            # middle track: no offset
            track_arcs_radii.append(r)
            # upper tracks
            for x in range(mid_track, track_count):
                track_offset = (x - lower_track_count) * self.TRK_WIDTH
                track_arcs_radii.append(r + track_offset)
        else:
	        # Even track count
            lower_track_count = int(track_count / 2)
            for x in range(lower_track_count):
                track_offset = (self.TRK_WIDTH / 2)  \
                        + ((lower_track_count - x -1) * self.TRK_WIDTH)
                track_arcs_radii.append((r - track_offset))
            for x in range(lower_track_count, track_count):
                track_offset = (self.TRK_WIDTH / 2) \
                        + (x - lower_track_count) * self.TRK_WIDTH
                track_arcs_radii.append(r + track_offset)
        cvrd_dist = 0
        total_dist = calc_sweep_dist(r, start_rads, end_rads)
        swp_total = calc_arc_sweep(start_rads, end_rads)
        for x in range(stripe_ct):
            track = self.rng.integers(track_count)
            stripe_len = self.rng.integers(
                    self.LOW_BND_LEN, self.UP_BND_LEN)
            cvrd_dist += (self.LOW_BND_LEN + self.UP_BND_LEN) / 4
            cvrd_prctg = round(cvrd_dist / total_dist, 3)
            begin_stripe = round(start_rads + (swp_total * cvrd_prctg), 3)
            end_prctg = round((cvrd_dist + stripe_len) / total_dist, 3)
            end_stripe = round(start_rads + (swp_total * end_prctg), 3)
            if end_stripe > end_rads:
                end_stripe = end_rads
            if begin_stripe > end_rads:
                continue
            stripe = draw_arc_path(ctr_pt, track_arcs_radii[track],
                    begin_stripe, end_stripe, self.RUT_WIDTH,
                    self.rut_color, dwg)
            stripe_grp.add(stripe)

    def compute_arc_rut_tracks(self, sweep_start, sweep_end, ctr_pt,
                                radius, dwg):
        """Controls drawing of curved ruts.

        If arc sweep crosses 0 degree threshold, this breaks the drawing
        into before and after 0 degree processing. Ported from the DOM
        canvas tile module.
        """
        arc_path_grp = dwg.g()
        if sweep_start > sweep_end:
            # Sweep passes over zero degrees, was needed for DOM canvas.
            sweep = calc_arc_sweep(sweep_start, sweep_end)
            swp_dist = calc_sweep_dist(radius, sweep_start, sweep_end)
            stripe_ct = int(math.ceil(swp_dist / (
                (self.LOW_BND_LEN + self.UP_BND_LEN) / 4)))
            first_part = ((2 * math.pi) - sweep_start) / sweep
            second_part = sweep_end / sweep
            self.draw_arc_path_ruts(ctr_pt, radius,
                    sweep_start, (2 * math.pi),
                    math.ceil(stripe_ct * first_part),
                    arc_path_grp, dwg)
            if sweep_end != 0:
                self.draw_arc_path_ruts(ctr_pt, radius,
                        0, sweep_end, math.ceil(stripe_ct * second_part),
                        arc_path_grp, dwg)
        else:
            sweep = math.degrees(sweep_end - sweep_start)
            swp_dist = calc_sweep_dist(radius, sweep_start, sweep_end)
            stripe_ct = int(math.ceil(swp_dist / (
                (self.LOW_BND_LEN + self.UP_BND_LEN) / 4)))
            self.draw_arc_path_ruts(ctr_pt, radius,
                    sweep_start, sweep_end, stripe_ct, arc_path_grp, dwg)
        return arc_path_grp

    def draw_clearing_ruts(self, ctr_pt, radius, dwg):
        """Draw series of curving wagon ruts over a clearing."""
        ruts_out = dwg.g()
        for ruts in self.CLRING_RUT_COORDS:
            start_rad = math.radians(ruts[0])
            end_rad = math.radians(ruts[1])
            ruts_out.add(draw_arc_path(ctr_pt, radius * ruts[2],
                    start_rad, end_rad, self.RUT_WIDTH,
                    self.rut_color, dwg))
        return ruts_out

    def draw_straight_path_ruts(self, ln_seg, path_width, dwg):
        """Draw wagon ruts over a straight path."""
        STRIPE_DIV = 10
        #TRACK_WIDTH = 9
        STRIPE_OFFSET_STEP = 10

        track_count = math.floor(path_width / self.TRK_WIDTH)
        path_len = round(math.dist(ln_seg[0], ln_seg[1]), 3)
        stripe_count = math.floor(path_len / STRIPE_DIV)
        angle = angle_two_pts(ln_seg[0], ln_seg[1])
        perpen_angle = perpendicular_angle(angle)
        track_lines_start = []
        track_lines_end = []
        lower_track_count = 0
        track_offset = 0
        mid_track = 0
        track = 0
        x = 0

        if track_count % 2 == 1:
            # Odd: subtract one from count, then evenly divide remaining
            # tracks between each side of centerline
            lower_track_count = int((track_count -1) / 2)
            mid_track = lower_track_count + 1
            for x in range(lower_track_count):
                track_offset = (lower_track_count - x) * self.TRK_WIDTH
                track_lines_start.append(calc_arc_pt(ln_seg[0],
                        -track_offset, perpen_angle))
                track_lines_end.append(calc_arc_pt(ln_seg[1],
                        -track_offset, perpen_angle))
            # middle track: no offset
            track_lines_start.append(ln_seg[0])
            track_lines_end.append(ln_seg[1])
            # upper tracks
            for x in range(mid_track, track_count):
                track_offset = (x - lower_track_count) * self.TRK_WIDTH
                track_lines_start.append(calc_arc_pt(ln_seg[0],
                        track_offset, perpen_angle))
                track_lines_end.append(calc_arc_pt(ln_seg[1],
                        track_offset, perpen_angle))
        else:
            # Evenly divide tracks btween each side of centerline:
            # no centerline track.
            lower_track_count = int(track_count / 2)
            for x in range(lower_track_count):
                track_offset = math.floor(self.TRK_WIDTH / 2) \
                        + ((lower_track_count - x -1) * self.TRK_WIDTH)
                track_lines_start.append(calc_arc_pt(ln_seg[0],
                        -track_offset, perpen_angle))
                track_lines_end.append(calc_arc_pt(ln_seg[1],
                        -track_offset, perpen_angle))
            for x in range(lower_track_count, track_count):
                track_offset = math.floor(self.TRK_WIDTH / 2) \
                        + (x - lower_track_count) * self.TRK_WIDTH
                track_lines_start.append(calc_arc_pt(ln_seg[0],
                        track_offset, perpen_angle))
                track_lines_end.append(calc_arc_pt(ln_seg[1],
                        track_offset, perpen_angle))

        stripe_start_offset = 0
        stripe_end_offset = 0
        stripe_len = 0
        stripe_start_pt = [0, 0]
        stripe_end_pt = [0, 0]
        stripe_grp = dwg.g()
        for x in range(stripe_count):
            track = self.rng.integers(0, track_count)
            stripe_len =  self.rng.integers(
                    self.LOW_BND_LEN, self.UP_BND_LEN)
            if stripe_start_offset > path_len:
                break
            if stripe_len + stripe_start_offset > path_len:
                stripe_end_offset = path_len
            else:
                stripe_end_offset = stripe_len + stripe_start_offset

            # compute start and end points for stripe centerline.
            stripe_start_pt = calc_arc_pt(
                    track_lines_start[track], stripe_start_offset, angle)
            stripe_end_pt = calc_arc_pt(
                    track_lines_start[track], stripe_end_offset, angle)
            stripe = create_cartes_path([stripe_start_pt, stripe_end_pt],
                    self.rut_color, self.RUT_WIDTH, dwg, 'round')
            stripe_grp.add(stripe)

            stripe_start_offset += STRIPE_OFFSET_STEP
        return stripe_grp


